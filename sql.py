#! /usr/bin/env python3

'''
All the SQL statements are here
'''

# # -------------------------------------------------------------------
# # IMPORTS
# # -------------------------------------------------------------------

import  sqlite3
import  my_utils
from    sqlite3     import Connection, Cursor
from    my_clases   import DNASequence

# # -------------------------------------------------------------------
# # FUNCTIONS
# # -------------------------------------------------------------------
# # -------------------------------------------------------------------
# # FUNCTION 1:

def create_table():
    '''SQL Table creation'''

    db_connection:      Connection  = sqlite3.connect('sequences.db')
    cursor_table:       Cursor      = db_connection.cursor()

    drop_statement:     str         = '''--sql
                                    DROP TABLE IF EXISTS dna_sequences_table;'''

    create_statement:   str         = '''--sql 
                                    CREATE TABLE dna_sequences_table ( 
                                        Sequence            TEXT,
                                        Length              INTEGER,
                                        A_Amount            INTEGER,
                                        A_perc              INTEGER,
                                        T_Amount            INTEGER,
                                        T_perc              INTEGER,
                                        C_Amount            INTEGER,
                                        C_perc              INTEGER,
                                        G_Amount            INTEGER,
                                        G_perc              INTEGER,
                                        Tm_°C               INTEGER,
                                        To_Protein          TEXT,
                                        Mol_Weight_kDa      REAL,
                                        Prot_GRAVY          REAL);
                                '''
    cursor_table.execute(drop_statement)
    cursor_table.execute(create_statement)
    db_connection.commit()
    db_connection.close()

# # -----------------------------------------------------------------------------
# # FUNCTION 2:

def insert_random_data_in_table():
    '''Inserting some random data to test and work'''

    db_connection:      Connection  = sqlite3.connect('sequences.db')
    cursor_table:       Cursor      = db_connection.cursor()
    insert_statement:   str         = '''INSERT INTO dna_sequences_table VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);'''
                          
    for _ in range(30):

        random_seq:             str         = my_utils.create_random_sequences()
        random_sequence_class:  DNASequence = DNASequence(random_seq)

        cursor_table.execute(insert_statement,
                                (random_sequence_class.dna_raw_sequence,
                                random_sequence_class.dna_length,
                                random_sequence_class.a,
                                random_sequence_class.a_perc,
                                random_sequence_class.t,
                                random_sequence_class.t_perc,
                                random_sequence_class.c,
                                random_sequence_class.c_perc,
                                random_sequence_class.g,
                                random_sequence_class.g_perc,
                                random_sequence_class.dna_melting_temperature,
                                random_sequence_class.dna_to_protein,
                                random_sequence_class.dna_protein_mol_weight_kDa,
                                random_sequence_class.dna_protein_gravy,))

    db_connection.commit()
    db_connection.close()

# # -----------------------------------------------------------------------------
# # FUNCTION 3:

def insert_user_data_in_table(sequence: DNASequence):
    '''Inserting the data that user wrote in the form to udpate data base'''

    db_connection:  Connection   = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor       = db_connection.cursor()

    cursor_tbl.execute('''INSERT INTO dna_sequences_table VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)''',
                                    (sequence.dna_60digits_sequence,
                                     sequence.dna_length,
                                     sequence.a,
                                     sequence.a_perc,
                                     sequence.t,
                                     sequence.t_perc,
                                     sequence.c,
                                     sequence.c_perc,
                                     sequence.g,
                                     sequence.g_perc,
                                     sequence.dna_melting_temperature,
                                     sequence.dna_to_protein,
                                     sequence.dna_protein_mol_weight_kDa,
                                     sequence.dna_protein_gravy))

    db_connection.commit()
    db_connection.close()

# # -----------------------------------------------------------------------------
# # FUNCTION 4:

def equal_to_filter(column_filter: str, rows_filter, checked_columns: str):

    db_connection:  Connection  = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor      = db_connection.cursor()

    drop_statement: str         =   ''' --sql
                                        DROP TABLE IF EXISTS xln_users_view
                                    ;'''

    cursor_tbl.execute(drop_statement)

    user_view_table             = cursor_tbl.execute(f'''
                                    CREATE TABLE xln_users_view AS
                                    SELECT {checked_columns} 
                                    FROM dna_sequences_table
                                    WHERE {column_filter}=:rows_filter;''',
                                    {'rows_filter':rows_filter})

    db_connection.commit()

    view_statement: str =   ''' --sql 
                                SELECT * FROM xln_users_view;
                            '''
    cursor_tbl.execute(view_statement)

    rows        = cursor_tbl.fetchall()

    db_connection.commit()
    db_connection.close()

    return rows

# # -----------------------------------------------------------------------------
# # FUNCTION 5:

def like_to_filter(column_filter: str, rows_filter, checked_columns: str):

    db_connection:  Connection       = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor           = db_connection.cursor()

    drop_statement: str              =   ''' --sql
                                             DROP TABLE IF EXISTS xln_users_view
                                        ;'''

    cursor_tbl.execute(drop_statement)
                                                
    user_view_table     = cursor_tbl.execute(f'''CREATE TABLE xln_users_view AS
                                    SELECT {checked_columns} 
                                    FROM dna_sequences_table
                                    WHERE {column_filter} LIKE (?)''',
                                    (f'''%{rows_filter}%''',))

    db_connection.commit()

    view_statement: str =   ''' --sql 
                                SELECT * FROM xln_users_view;
                            '''
    cursor_tbl.execute(view_statement)

    rows        = cursor_tbl.fetchall()

    db_connection.commit()
    db_connection.close()

    return rows

# # -----------------------------------------------------------------------------
# # FUNCTION 6:

def bigger_than_filter(column_filter: str, rows_filter, checked_columns: str):

    db_connection:  Connection    = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor        = db_connection.cursor()

    drop_statement: str           =   ''' --sql
                                          DROP TABLE IF EXISTS xln_users_view
                                      ;'''

    cursor_tbl.execute(drop_statement)
                                                
    user_view_table     = cursor_tbl.execute(f'''CREATE TABLE xln_users_view AS
                                     SELECT {checked_columns} 
                                    FROM dna_sequences_table
                                    WHERE {column_filter}>:rows_filter;''',
                                    {'rows_filter':rows_filter})

    db_connection.commit()

    view_statement: str =   ''' --sql 
                                SELECT * FROM xln_users_view;
                            '''
    cursor_tbl.execute(view_statement)

    rows        = cursor_tbl.fetchall()

    db_connection.commit()
    db_connection.close()

    return rows

# # -----------------------------------------------------------------------------
# # FUNCTION 7:

def less_than_filter(column_filter: str, rows_filter, checked_columns: str):

    db_connection:  Connection    = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor        = db_connection.cursor()

    drop_statement: str           =   ''' --sql
                                          DROP TABLE IF EXISTS xln_users_view
                                      ;'''

    cursor_tbl.execute(drop_statement)
                                                
    user_view_table     = cursor_tbl.execute(f'''CREATE TABLE xln_users_view AS
                                     SELECT {checked_columns} 
                                    FROM dna_sequences_table
                                    WHERE {column_filter}<:rows_filter;''',
                                    {'rows_filter':rows_filter})

    db_connection.commit()

    view_statement: str =   ''' --sql 
                                SELECT * FROM xln_users_view;
                            '''
    cursor_tbl.execute(view_statement)

    rows        = cursor_tbl.fetchall()

    db_connection.commit()
    db_connection.close()

    return rows

# # -----------------------------------------------------------------------------
# # FUNCTION 7:

def bigger_or_equal_filter(column_filter: str, rows_filter, checked_columns: str):

    db_connection:  Connection    = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor        = db_connection.cursor()

    drop_statement: str           =   ''' --sql
                                          DROP TABLE IF EXISTS xln_users_view
                                      ;'''

    cursor_tbl.execute(drop_statement)
                                                
    user_view_table     = cursor_tbl.execute(f'''CREATE TABLE xln_users_view AS
                                     SELECT {checked_columns} 
                                    FROM dna_sequences_table
                                    WHERE {column_filter}>=:rows_filter;''',
                                    {'rows_filter':rows_filter})

    db_connection.commit()

    view_statement: str =   ''' --sql 
                                SELECT * FROM xln_users_view;
                            '''
    cursor_tbl.execute(view_statement)

    rows        = cursor_tbl.fetchall()

    db_connection.commit()
    db_connection.close()

    return rows

# # -----------------------------------------------------------------------------
# # FUNCTION 7:

def less_or_equal_filter(column_filter: str, rows_filter, checked_columns: str):

    db_connection:  Connection    = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor        = db_connection.cursor()

    drop_statement: str           =   ''' --sql
                                          DROP TABLE IF EXISTS xln_users_view
                                      ;'''

    cursor_tbl.execute(drop_statement)
                                                
    user_view_table     = cursor_tbl.execute(f'''CREATE TABLE xln_users_view AS
                                     SELECT {checked_columns} 
                                    FROM dna_sequences_table
                                    WHERE {column_filter}<=:rows_filter;''',
                                    {'rows_filter':rows_filter})

    db_connection.commit()

    view_statement: str =   ''' --sql 
                                SELECT * FROM xln_users_view;
                            '''
    cursor_tbl.execute(view_statement)

    rows        = cursor_tbl.fetchall()

    db_connection.commit()
    db_connection.close()

    return rows

# # -----------------------------------------------------------------------------
# # FUNCTION 7:

def different_to_filter(column_filter: str, rows_filter, checked_columns: str):

    db_connection:  Connection    = sqlite3.connect('sequences.db')
    cursor_tbl:     Cursor        = db_connection.cursor()

    drop_statement: str           =   ''' --sql
                                          DROP TABLE IF EXISTS xln_users_view
                                      ;'''

    cursor_tbl.execute(drop_statement)
                                                
    user_view_table     = cursor_tbl.execute(f'''CREATE TABLE xln_users_view AS
                                     SELECT {checked_columns} 
                                    FROM dna_sequences_table
                                    WHERE {column_filter}<> :rows_filter;''',
                                    {'rows_filter':rows_filter})

    db_connection.commit()

    view_statement: str =   ''' --sql 
                                SELECT * FROM xln_users_view;
                            '''
    cursor_tbl.execute(view_statement)

    rows        = cursor_tbl.fetchall()

    db_connection.commit()
    db_connection.close()

    return rows