#! /usr/bin/env python3

''''
Here there are all functions used in the app_main.py
'''

#----------------------------------------------------------------------
# Imports
#----------------------------------------------------------------------

from    flask       import  redirect,    render_template
from    copy        import  copy
from    my_clases   import  DNASequence, SeqReader
from    pathlib     import  Path
import  numpy               as      np
import  matplotlib.pyplot   as      plt
import  pandas
import  random
import  sqlite3

import  sql
import  pprint

#######################################################################
#----------------------------------------------------------------------
# FUNCTIONS FOR MANAGES SEQUENCES
#----------------------------------------------------------------------
# Function 1:

def create_html_index_get_method() -> str:
    
    random_dna:                 str = 'Random DNA Sequences'
    random_prot:                str = 'Random Protein Sequences'
    complement_header:          str = 'Reverse & Complement'
    dna_translations:           str = 'DNA Translate'
    seq_cutter:                 str = 'DNA Range Extractor'
    combine_sequences:          str = 'Combine sequences'
    validate_dna:               str = 'Validate DNA'
    validate_protein:           str = 'Validate Protein'
    filter_validate_dna:        str = 'Filter & Validate DNA'
    filter_validate_protein:    str = 'Filter & Validate Protein'
    dna_mol_weight:             str = 'DNA Molecular Weight'
    dna_to_prot_gravy:          str = 'DNA to Protein GRAVY'
    prot_mol_weight:            str = 'Protein Molecular Weight'
    prot_gravy:                 str = 'Protein GRAVY'
    nucleotides_amount:         str = 'Nucleotides Amount'
    codons_amount:              str = 'Codons Amount'
    find_pattern:               str = 'Find Pattern'
    tools:                      str = 'Experimental Tools'
    qpcr:                       str = 'IPH Tool'
    index_html:                 str = render_template('index.html',
                                        title                       = 'FLASK - X.Llobet',
                                        header_dna                  = 'DNA sequences',
                                        header_protein              = 'Protein sequences',
                                        header_db                   = 'DNA Sequences Data Base',
                                        header_random_seqs          = 'Random Sequences',
                                        header_about                = 'About this site',
                                        rand_dna_header             = random_dna,
                                        rand_prot_header            = random_prot,
                                        compl_header                = complement_header,
                                        translate_header            = dna_translations,
                                        seq_cutter_header           = seq_cutter,
                                        combine_seq_header          = combine_sequences,
                                        validate_dna_header         = validate_dna,
                                        validate_prot_header        = validate_protein,
                                        filt_val_dna_header         = filter_validate_dna,
                                        filt_val_prot_header        = filter_validate_protein,
                                        dna_mol_weight_header       = dna_mol_weight,
                                        dna_to_prot_gravy_header    = dna_to_prot_gravy,
                                        prot_mol_weight_header      = prot_mol_weight,
                                        prot_gravy_header           = prot_gravy,
                                        nucleotides_header          = nucleotides_amount,
                                        codons_header               = codons_amount,
                                        find_pattern_header         = find_pattern,
                                        tools_header                = tools,
                                        qpcr_header                 = qpcr)

    return index_html

#----------------------------------------------------------------------
# Function 2:

def create_60bp_dna_sequence() -> str:
    '''Creates a random DNA sequence of 60bp, and return it as a fasta structure'''

    nucleotides_type:   list[str]   = ["A","T","C","G"]
    seq_60bp:           str         = ''

    for _ in range(60):
        nucleotide: str = random.choice(nucleotides_type)
        seq_60bp        = seq_60bp + nucleotide
    
    assert (seq_60bp != '') 

    return seq_60bp 

#----------------------------------------------------------------------
# Function 3:

def create_120bp_dna_sequence() -> str:
    '''Creates a random DNA sequence of 120bp, and return it as a fasta structure'''

    first_seq:   str = create_60bp_dna_sequence()
    second_seq:  str = create_60bp_dna_sequence()

    assert first_seq.isalpha() and second_seq.isalpha()

    seq_120bp:   str = f'''{first_seq}{second_seq}'''

    return seq_120bp

#----------------------------------------------------------------------
# Function 4:

def create_180bp_dna_sequence() -> str:
    '''Creates a random DNA sequence of 180bp, and return it as a fasta structure'''

    seq_180bp:   str = f''

    for _ in range(3):

        random_seq_fa:  str = create_60bp_dna_sequence()
        seq_180bp           = seq_180bp + random_seq_fa

    return seq_180bp

#----------------------------------------------------------------------
# Function 5:

def create_600bp_dna_sequence() -> str:
    '''Creates a random DNA sequence of 600bp, and return it as a fasta structure'''

    seq_600bp:   str = f''

    for _ in range(10):

        random_seq_fa:  str = create_60bp_dna_sequence()
        seq_600bp           = seq_600bp + random_seq_fa
    
    return seq_600bp

#----------------------------------------------------------------------
# Function 6:

def create_6000bp_dna_sequence() -> str:
    '''Creates a random DNA sequence of 6000bp, and return it as a fasta structure'''

    seq_6000bp:  str = f''

    for _ in range(100):

        random_seq_fa:  str = create_60bp_dna_sequence()
        seq_6000bp          = seq_6000bp + random_seq_fa
    
    return seq_6000bp

#----------------------------------------------------------------------
# Function 7:

def user_dna_seq(number_of_lines: int) -> str:
    '''Creates a random DNA sequence of specified (from user) lines of 
    60bp each, and return it as a fasta structure'''

    # Precondition:
    assert (number_of_lines > 0), "Didn't insert a number of lines"   

    user_seq:    str = f''

    for _ in range(number_of_lines):
        
        random_seq_fa:   str = create_60bp_dna_sequence()
        user_seq              = user_seq + random_seq_fa

    return user_seq

#----------------------------------------------------------------------
# Function 8:

def create_random_sequences() -> str:
    '''Creates a random DNA sequence of 30bp, and return it as a fasta structure'''

    nucleotides_type:   list[str]   = ["A","T","C","G"]
    seq_30bp:           str         = ''

    for _ in range(30):
        nucleotide: str = random.choice(nucleotides_type)
        seq_30bp        = seq_30bp + nucleotide
    
    assert (seq_30bp != '') 

    return seq_30bp

#----------------------------------------------------------------------
# Function 9:

def create_30bp_protein_sequence() -> str:

    nucleotides_type:   list[str]   = ['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','*']
    seq_60bp:           str         = ''

    for _ in range(30):
        
        nucleotide:     str = random.choice(nucleotides_type)
        seq_60bp            = seq_60bp + nucleotide

    assert (seq_60bp != '') 

    return seq_60bp

#----------------------------------------------------------------------
# Function 9:

def create_60bp_protein_sequence() -> str:

    nucleotides_type:   list[str]   = ['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','*']
    seq_60bp:           str         = ''

    for _ in range(60):
        
        nucleotide:     str = random.choice(nucleotides_type)
        seq_60bp            = seq_60bp + nucleotide

    assert (seq_60bp != '') 

    return seq_60bp

#----------------------------------------------------------------------
# Function 8:

def create_120bp_protein_sequence() -> str:
    '''Creates a random DNA sequence of 120bp, and return it as a fasta structure'''


    first_seq_fa:   str = create_60bp_protein_sequence()
    second_seq_fa:  str = create_60bp_protein_sequence()


    seq_120bp:      str = f'{first_seq_fa}{second_seq_fa}'


    return seq_120bp

#----------------------------------------------------------------------
# Function 9:

def create_180bp_protein_sequence() -> str:
    '''Creates a random DNA sequence of 180bp, and return it as a fasta structure'''

    seq_180bp:  str = f''

    for _ in range(3):
        random_seq_fa:      str = create_60bp_protein_sequence()
        seq_180bp               = seq_180bp + random_seq_fa
    

    return seq_180bp

#----------------------------------------------------------------------
# Function 10:

def create_600bp_protein_sequence() -> str:
    '''Creates a random DNA sequence of 600bp, and return it as a fasta structure'''

    seq_600bp:  str = f''

    for _ in range(10):

        random_seq_fa:      str = create_60bp_protein_sequence()
        seq_600bp               = seq_600bp + random_seq_fa

    return seq_600bp

#----------------------------------------------------------------------
# Function 11:

def create_6000bp_protein_sequence() -> str:
    '''Creates a random DNA sequence of 6000bp, and return it as a fasta structure'''

    seq_6000bp:  str = f''

    for _ in range(100):
        random_seq_fa:      str = create_60bp_protein_sequence()
        seq_6000bp              = seq_6000bp + random_seq_fa
    
    return seq_6000bp

#----------------------------------------------------------------------
# Function 12:

def user_protein_seq(number_of_lines: int) -> str:
    '''Creates a random DNA sequence of specified (from user) lines of 
    60bp each, and return it as a fasta structure'''

    # Precondition:
    assert (number_of_lines > 0), "Didn't insert a number of lines"   

    user_seq_string:    str = f''

    for _ in range(number_of_lines):
        
        random_seq_fa:      str = create_60bp_protein_sequence()
        user_seq_string         = user_seq_string + random_seq_fa

    return user_seq_string

#----------------------------------------------------------------------
# Function 13:

def create_60_digits_format(entry_sequence: str) -> str:
    '''Manipulates only a entry sequence of, doesn't matter what,
    dona, proteins, etc....and return only the entry sequence, but
    with until 60 digits for each line.'''

    no_row_sequence_slides: str = entry_sequence.replace('\r\n', '')
    no_tab_sequence: str = no_row_sequence_slides.replace('\t', '')
    raw_sequence: str = no_tab_sequence.replace('\n', '')
    entry_sequence_list: list[str] = list(raw_sequence.replace(' ', ''))

    body:            str        = ''        
    count:           int        = 0
    nucelotide:      str

    for nucleotide in entry_sequence_list:
        body  = body  + nucleotide
        count = count + 1

        if count == 60 and not body.endswith('\n'):

            body  = body  + '\n'
            count = 0

    return body

#----------------------------------------------------------------------
# Function 14:

def change_thymine_to_uracil(entry_sequence: str) -> str:
    '''This function recieve an entry sequence or entry fasta of nucleotides,
    and change all the T (thymine) for U (uracil)'''
    
    # Precondition:
    assert (entry_sequence != ''), "No sequence to change T for U. Your entry data is empty!"

    header:                     str                     = ' RNA sequence with lengh of '
    rna_sequence_string:   str                     = entry_sequence.replace('T', 'U')
    
    # Postcondition:
    assert (rna_sequence_string != '') and (rna_sequence_string.count('T') == 0), "There's NO sequence to return or, still 'T' in sequence product"

    return rna_sequence_string  

#----------------------------------------------------------------------
# Function 15:

def sequence_cutter(entry_sequence: str, start: int, end: int) -> str:
    '''This function recieve an entry sequence or entry fasta of nucleotides, the 
    start-cut and the end-cut in the entry sequence, and substract the slice to a new 
    sequence of nuclotides. Return it as a FASTA structure'''

    # Precondition:
    assert (entry_sequence != ''), "No sequence to cut. Your entry data is empty!"

    prepared_sequence: str = prepare_sequence(entry_sequence)
    
    nucleotide_list:        list[str]   = list(prepared_sequence)
    new_nucleotide_list:    list[str]   = nucleotide_list[start-1:end]
    new_sequence:           str         = ''.join(new_nucleotide_list)

    new_sequence_prepared:  str         = prepare_sequence(new_sequence)

    # Postcondition:
    assert (new_sequence_prepared != ''), "There's No sequence to return" 

    return new_sequence_prepared.upper()

#----------------------------------------------------------------------
# Function 16:

def combine_sequences(entry_data_1: str, entry_data_2: str) -> str:
    '''Recieve two sequences or two fasta strings, and combine both sequences.
    Joins the second sequence after the firs sequence, and returns the
    new sequence as fasta structure.'''

    # Precondition:
    assert (entry_data_1 != '') and (entry_data_2 != ''), "One or both sequences are empty."

    new_seq:        str     = (entry_data_1 + entry_data_2).replace('\n', '')

    # Postcondition:
    assert (new_seq != ''), "There's No sequence to return"     
       
    return new_seq

#----------------------------------------------------------------------
# Set of helper functions for dna_validator() in the follow code

# Function 17:
#------------

def prepare_sequence(entry_sequence: str) -> str:
    '''Susbtitute spaces and breaking lines for an empty string'''

    raw_sequence:           str     = entry_sequence.replace('\n', '')
    raw_sequence_no_rows:   str     = raw_sequence.replace('\r', '')
    checked_sequence:       str     = raw_sequence_no_rows.replace(' ', '').upper()

    return checked_sequence

# Function 18:
#------------

def ckeck_dna_sequence(entry_dna_seq: str) -> tuple:
    '''Checl if the entry sequence is DNA or not, and return a list od
    fails, and a counter. If counter is negative, it means that is not
    a DNA sequence.'''


    checker:                            str         = 'atcgrynATCGRYN'
    fails:                              list        = []
    nucleotide_index_counter:           int         = 1
    counter:                            int         = 0

    for nucleotide in entry_dna_seq:

        finder:                         int         = checker.find(nucleotide)
        nucleotide_in:                  bool        = finder >= 0
        nucleotide_not_in:              bool        = finder < 0

        if nucleotide_in:
            pass

        if nucleotide_not_in:
            counter = counter + finder
            fails.append(nucleotide_index_counter)

            # if len(fails) == 60:
            #     fails.append('\n')

        nucleotide_index_counter= nucleotide_index_counter + 1

    return fails, counter 

# Function 19:
#------------

def ckeck_prot_sequence(entry_protein_seq: str) -> tuple:
    '''Check if the entry sequence is DNA or not, and return a list od
    fails, and a counter. If counter is negative, it means that is not
    a DNA sequence.'''


    checker:                            str         = 'ACDEFGHIKLMNPQRSTVWY*^'
    fails:                              list        = []
    nucleotide_index_counter:           int         = 1
    counter:                            int         = 0

    for nucleotide in entry_protein_seq:

        finder:                         int         = checker.find(nucleotide)
        nucleotide_in:                  bool        = finder >= 0
        nucleotide_not_in:              bool        = finder < 0

        if nucleotide_in:
            pass

        if nucleotide_not_in:
            counter = counter + finder
            fails.append(nucleotide_index_counter)

            if len(fails) == 60:
                fails.append('\n')

        nucleotide_index_counter= nucleotide_index_counter + 1

    return fails, counter 

 # Function 20:
 #------------

def generate_sequence_errors(entry_dna_seq: str, fails_list:list) -> list[str]:
    
    length_seq:                     int         = len(entry_dna_seq.replace('\n', ''))
    empty_sequence:                 str         = ('_')*length_seq
    errors_sequence_list:           list[str]   = list(empty_sequence)

    for index in fails_list:

        if fails_list == []:
                pass
            
        if fails_list != []:

            if index != '\n':
                #empty_sequence_list[index].replace(' ', '^')
                errors_sequence_list.pop(index-1)
                errors_sequence_list.insert(index-1, '|')
                
                if index == '\n':
                    pass

    error_marks_string:             str         = ''.join(errors_sequence_list)
    formatted_error_marks_string:   str         = create_60_digits_format(error_marks_string)
    formatted_error_marks_list:     list[str]   = formatted_error_marks_string.split('\n')
    
    return formatted_error_marks_list

# Function 21:
#------------

def group_sequence_with_errors(entry_dna_seq: list[str], fails_list:list) -> str:

    zipped_results                               = zip(entry_dna_seq, fails_list)
    zipped_results_list:            list[str]    = []

    for slice_seq, slice_error in zipped_results:
        zipped_results_list.append(slice_seq)
        zipped_results_list.append(slice_error)

    result_sequence: str = '\n'.join(zipped_results_list)

    return result_sequence

#----------------------------------------------------------------------
# Function 22:

def dna_validator(dna_sequence: str):
    '''
    Recieve as Input a DNA sequence, NOT IN FASTA structure and depending the
    data can return:
       1- Validated DNA sequence, or
       2- Message error
    '''
    
    entry_dna_sequence:   str  = prepare_sequence(dna_sequence)
    fails, counter             = ckeck_dna_sequence(entry_dna_sequence)
    is_sequence:          bool = counter == 0
    is_not_sequence:      bool = counter != 0

    if is_sequence:

        valid_dna_sequence:  str = create_60_digits_format(entry_dna_sequence)

        return valid_dna_sequence
    
    if is_not_sequence:

        formatted_adn_sequence:         str          = create_60_digits_format(entry_dna_sequence)
        formatted_adn_sequence_list:    list[str]    = formatted_adn_sequence.split('\n')
        errors_sequence_list:           list[str]    = generate_sequence_errors(formatted_adn_sequence, fails)
        result_sequence:                str          = group_sequence_with_errors(formatted_adn_sequence_list, errors_sequence_list)
        #fails_result: str = format_fails_output(fails)
        fail_message:                   str         = f"\n> Some nucleotides are not 'A','T','C','G','R','Y' or 'N' in the entry sequence, at positions:\n{fails}\n> Errors map (black rows):\n{result_sequence}"
        
        pprint.pp(formatted_adn_sequence_list)
        return fail_message

#----------------------------------------------------------------------
# Function 23:

def protein_validator(protein_sequence: str):
    '''
    Recieve as Input a string as a entry sequence, and depending the
    data can return:
       1- Validated DNA sequence, or
       2- Message error
    '''

    entry_prot_sequence:   str  = prepare_sequence(protein_sequence)
    fails, counter              = ckeck_prot_sequence(entry_prot_sequence)
    is_sequence:          bool = counter == 0
    is_not_sequence:      bool = counter != 0

    if is_sequence:

        valid_dna_sequence:  str = create_60_digits_format(entry_prot_sequence)

        return valid_dna_sequence
    
    if is_not_sequence:

        formatted_adn_sequence:         str          = create_60_digits_format(entry_prot_sequence)
        formatted_adn_sequence_list:    list[str]    = formatted_adn_sequence.split('\n')
        errors_sequence_list:           list[str]    = generate_sequence_errors(entry_prot_sequence, fails)
        result_sequence:                str          = group_sequence_with_errors(formatted_adn_sequence_list, errors_sequence_list)
        #fails_result: str = format_fails_output(fails)
        fail_message2:                   str         = f"\n> Some nucleotides are not 'A','T','C','G' or 'U' in the entry sequence, at positions:\n{fails}\n> ERRORS:\n{result_sequence}"
        fail_message:                    str         = f"\n> Some AmAcids are not 'ACDEFGHIKLMNPQRSTVWY' in entry sequence, at positions:\n{fails}\n> Filtered sequence with length {len(entry_prot_sequence)}:\n{result_sequence}"
        return fail_message

#----------------------------------------------------------------------
# Function 24:

def dna_filter(entry_data: str):
    '''This function recieves a entry DNA string sequence, NOT FASTA,
    and remove all numeric digits and all the free spaces'''

    # Manipulating entry data:
    upper_entry_data:               str         = entry_data.upper()
    stripped_upper_entry_data_v1:   str         = upper_entry_data.replace(' ', '')
    stripped_upper_entry_data_v2:   str         = stripped_upper_entry_data_v1.replace('\n', '')
    stripped_upper_entry_data_v3:   str         = stripped_upper_entry_data_v2.replace('\t', '')
    filtered_dna_list:              list[str]   = [digit for digit in stripped_upper_entry_data_v3 if digit.isalpha()]

    # Filtered alphabetic data
    filtered_dna:                   str         = ''.join(filtered_dna_list)
    validated_dna_sequence:         str         = dna_validator(filtered_dna)
    header:                         str         = ' Filtered DNA sequence with length of '

    # If validated DNA sequence starts by '\n', it means that is NOT a
    # validated DNA sequence beacause some digits are NOT A,T,C,G or U.
    if validated_dna_sequence.startswith('\n>'):
        return validated_dna_sequence

    # If validated DNA sequence DOESN'T starts by '\n', it means that IS  a
    # validated DNA sequence beacause ALL digits are A,T,C,G or U.
    if not validated_dna_sequence.startswith('\n>'):        
        validated_dna_fa: str = create_60_digits_format(validated_dna_sequence)
        return validated_dna_fa

#----------------------------------------------------------------------
# Function 25:

def protein_filter(entry_data: str):
    '''This function recieves a entry PROTEIN string sequence, an remove 
    all numeric digits and oll the free spaces'''

    checker: str = 'ACDEFGHIKLMNPQRSTVWY-*'

       # Manipulating entry data:
    upper_entry_data:               str         = entry_data.upper()
    stripped_upper_entry_data_v1:   str         = upper_entry_data.replace(' ', '')
    stripped_upper_entry_data_v2:   str         = stripped_upper_entry_data_v1.replace('\n', '')
    stripped_upper_entry_data_v3:   str         = stripped_upper_entry_data_v2.replace('\t', '')
    filtered_proteins_list:         list[str]   = [digit for digit in stripped_upper_entry_data_v3 if digit in checker]

    # Filtered alphabetic data
    filtered_proteins:              str         = ''.join(filtered_proteins_list)
    validated_protein_sequence:     str         = protein_validator(filtered_proteins)
    header:                         str         = 'Filtered protein sequence with length of '

    # If validated PROTEIN sequence starts by '\n', it means that is NOT a
    # validated PROTEIN sequence beacause some digits are NOT A,C,D,E,F,G,H,
    # I,K,L,M,N,P,Q,R,S,T,V,W or Y.
    if validated_protein_sequence.startswith('\n>'):
        return validated_protein_sequence

    # If validated PROTEIN sequence DOESN'T starts by '\n', it means that IS a
    # validated PROTEIN sequence beacause ALL digits are A,C,D,E,F,G,H,I,K,L,M,
    # N,P,Q,R,S,T,V,W or Y.
    if not validated_protein_sequence.startswith('\n>'):
        validated_proteins_fa: str = create_60_digits_format(filtered_proteins)
        return validated_proteins_fa

#----------------------------------------------------------------------
# Function 26:

def field_to_redirect(field: str):

    return redirect(field)

#----------------------------------------------------------------------
# Function 27:

def find_pattern_indexes_in_string(entry_data: str,pattern: str):
    '''
    Input: Can be even a string as a FASTA structure or sequence string.

    Searches a pattern into an entry data, and returns a list of integers
    indicating the position where the pattern is into the entry data. If
    pattern is in several times, return a lis of lists of integers, indicating
    the start and the finish position por each time the pattern was found.
    '''

    # Precondition:
    assert (entry_data != '') and (pattern != ''), "Pattern or entry data are empty!"

    upper_pattern:              str         = pattern.upper()
    sequence_class:             SeqReader   = SeqReader(entry_data)
    #entry_sequence:             str         = xlnseq_fasta.extract_only_dna_sequence_from_fasta(entry_data.upper())
    raw_sequence:               str         = sequence_class.raw_sequence
    raw_seq_with_patterns:      str         = copy(raw_sequence)
    pattern_repeats:            int         = raw_sequence.count(upper_pattern)
    pattern_length:             int         = len(upper_pattern)    
    pattern_indexes_list:       list        = []
    pattern_is_single_digit:    bool        = (pattern_length == 1)
    pattern_is_multiple_digit:  bool        = (pattern_length > 1)    
        
    if upper_pattern in raw_sequence:        

            for _ in range(pattern_repeats):

                start_index:         int        = raw_sequence.find(upper_pattern)
                end_index:           int        = start_index + pattern_length
                start_end_positions: list[int]  = [start_index + 1, end_index]
                pattern_indexes_list.append(start_end_positions)
                raw_seq_with_patterns             = raw_seq_with_patterns.replace(upper_pattern, '^'*pattern_length, 1)
            
            multiple_patterns_matched: str = f'''> Start and end of '{upper_pattern}' pattern positions:\n {pattern_indexes_list}\n\n> Entered sequence:\n{raw_seq_with_patterns}\n\n> Pattern replaced by '^':\n{raw_sequence}'''
            
            # Postcondition:
            #assert (len(entry_sequence)) == (len(raw_sequence))

            return raw_seq_with_patterns

    if upper_pattern not in raw_sequence:
        
        error_string: str = '> Your pattern is NOT in the sequence'
        return(error_string)


#######################################################################
#----------------------------------------------------------------------
# FUNCTIONS FOR DATA BASE
#----------------------------------------------------------------------

# DB Function 1:
def filter_table_data(column_filter: str, filter_value: str, rows_filter, checks_list: list[str]) -> str:


    if 'ALL' in checks_list:
        checks_list = ['Sequence','Length','A_Amount','A_perc','T_Amount','T_perc','C_Amount','C_perc','G_Amount','G_perc','Tm_°C','To_Protein','Mol_Weight_kDa','Prot_GRAVY']


    db_connection           = sqlite3.connect('sequences.db')
    cursor_tbl              = db_connection.cursor()
    checked_columns: str    = ','.join(checks_list)

    if (column_filter != 'Sequence') and (column_filter != 'To_Protein') and (column_filter != 'Protein_GRAVY') and (column_filter != 'Mol_Weight_kDa'):

        rows_filter = int(rows_filter)

    if (column_filter == 'Mol_Weight_kDa') or (column_filter == 'Protein_GRAVY'):

        rows_filter = float(rows_filter)

    if (column_filter == 'Sequence'):

        sequence_class: DNASequence = DNASequence(rows_filter)
        rows_filter = sequence_class.dna_60digits_sequence

    else:
        pass

    show_all_columns:   bool = 'ALL' in checks_list
    show_some_columns:  bool = 'ALL' not in checks_list

    if filter_value == 'equal to':                

        rows                    = sql.equal_to_filter(column_filter, rows_filter, checked_columns)
        table_html_post1: str    = render_template('get_dna_table.html',
                                                    title = 'DNA Table',
                                                    entry_columns = checks_list,
                                                    entry_rows = rows)
        return table_html_post1

    if filter_value == 'like':                

        rows                    = sql.like_to_filter(column_filter, rows_filter, checked_columns)
        table_html_post2: str    = render_template('get_dna_table.html',
                                                    title = 'DNA Table',
                                                    entry_columns = checks_list,
                                                    entry_rows = rows)
        return table_html_post2

    if filter_value == 'bigger than':           

        rows                    = sql.bigger_than_filter(column_filter, rows_filter, checked_columns)
        table_html_post3: str    = render_template('get_dna_table.html',
                                                    title = 'DNA Table',
                                                    entry_columns = checks_list,
                                                    entry_rows = rows)
        return table_html_post3

    if filter_value == 'less than':           

        rows                    = sql.less_than_filter(column_filter, rows_filter, checked_columns)
        table_html_post4: str    = render_template('get_dna_table.html',
                                                    title = 'DNA Table',
                                                    entry_columns = checks_list,
                                                    entry_rows = rows)
        db_connection.close()
        return table_html_post4

    if filter_value == 'bigger or equal':           

        rows                    = sql.bigger_or_equal_filter(column_filter, rows_filter, checked_columns)
        table_html_post5: str    = render_template('get_dna_table.html',
                                                    title = 'DNA Table',
                                                    entry_columns = checks_list,
                                                    entry_rows = rows)
        db_connection.close()
        return table_html_post5

    if filter_value == 'less or equal':           

        rows                    = sql.less_or_equal_filter(column_filter, rows_filter, checked_columns)
        table_html_post6: str    = render_template('get_dna_table.html',
                                                    title = 'DNA Table',
                                                    entry_columns = checks_list,
                                                    entry_rows = rows)
        db_connection.close()
        return table_html_post6

    if filter_value == 'different':           

        rows                    = sql.different_to_filter(column_filter, rows_filter, checked_columns)
        table_html_post7: str    = render_template('get_dna_table.html',
                                                    title = 'DNA Table',
                                                    entry_columns = checks_list,
                                                    entry_rows = rows)
        db_connection.close()
        return table_html_post7

    else:
        return '''Couldn't find rows'''

#######################################################################
#----------------------------------------------------------------------
# FUNCTIONS FOR CREATING FILES FOR DOWNLOADING
#----------------------------------------------------------------------

# DB Function 1:
def create_files():

    pandas_html_table: pandas.DataFrame = pandas.read_html('http://localhost:5000/get_dna_table.html')[0]

    pandas_html_table.to_excel('dna_table.xlsx')
    pandas_html_table.to_csv('dna_table.csv')
    pandas_html_table.to_json('dna_table.json')
    pandas_html_table.to_html('dna_table.html')

#----------------------------------------------------------------------

# DB Function 2:
def create_fasta_single_sequence_file_from_html(name_of_file: str, header:str, html_string: str,):
    '''Recieves three parameters, the name of the new file were to write the fasta sequence,
    the header of the sequence and the html as string where to extract the sequence from (from single fasta output).
    Generates a new file with the fasta (header + sequence) wrote in it.'''

    file_txt = open(name_of_file, 'w')

    html_first_split:   list[str]   = html_string.split('</p>')
    html_second_split:  list[str]   = html_first_split[1].split('</div>')
    html_third_split:   str         = html_second_split[0]
    html_fourth_split:  str         = html_third_split.replace(' ', '')
    html_fifth_split:   str         = html_fourth_split.replace('\n', '')
    html_sixth_split:   str         = html_fifth_split.replace('<br>', '\n').strip()

    file_txt.write(header)
    file_txt.write(html_sixth_split)
    file_txt.close()

#----------------------------------------------------------------------

# DB Function 3:
def create_fasta_multiple_sequence_file_from_html(name_of_file: str, header:str, html_string: str,):
    '''Recieves three parameters, the name of the new file were to write the fasta sequence,
    the header of the sequence and the html as string where to extract the sequence from (from multiple fasta output).
    Generates a new file with the fasta (header + sequence) wrote in it.'''

    file_txt = open(name_of_file, 'w')

    html_first_split:           list[str]   = html_string.split('</form><br><br><br>')
    html_second_split:          list[str]   = html_first_split[1].split('</div>')
    html_third_split:           list[str]   = html_second_split[0].split('&gt;')
    html_fourth_split:          list[str]   = html_third_split[len(html_third_split)-1].split('</p>')
    reverse_seq_html_output:    str         = html_fourth_split[1]
    html_fifth_split:           str         = reverse_seq_html_output.replace(' ', '')
    html_sixth_split:           str         = html_fifth_split.replace('\n', '')
    html_seventh_split:         str         = html_sixth_split.replace('<br>', '\n')
    html_eight_split:           str         = html_seventh_split.replace('</p>', '\n').strip()

    file_txt.write(header)
    file_txt.write(html_eight_split)
    file_txt.close()

#----------------------------------------------------------------------

# DB Function 4:
def write_txt_file(name_of_file: str, text_body:str):

    file_txt = open(name_of_file, 'w')

    file_txt.write(text_body)
    file_txt.close()

#######################################################################
#----------------------------------------------------------------------
# RENDER TEMPLATES FUNCTIONS
#----------------------------------------------------------------------

def render_rev_compl_html_template(input_dna_sequence: str, work_sequence: str, entry_title: str) -> str:

    sequence_class:         DNASequence     = DNASequence(input_dna_sequence)
    work_sequence_class:    DNASequence     = DNASequence(work_sequence)

    second_header:          str             = f'''> {entry_title} sequence with length of {sequence_class.dna_length}:'''
    headers_list:           list[str]       = ['> Entered sequence:',  second_header]
    sequences_rows:         list[list[str]] = []

    sequences_rows.append(sequence_class.dna_60digits_sequence.split('\n'))
    sequences_rows.append(work_sequence_class.dna_60digits_sequence.split('\n'))

    output_data                             = zip(headers_list, sequences_rows)


    sequence_html_output_revcomp:   str             = render_template('get_post_rev_comp_revcomp.html',
                                                                        title  = entry_title,
                                                                        data   = output_data,)
                
    ## GENERATING FASTA ######################################                    
    sequence_file_name_revcomp:     str             = f'''{entry_title}_sequence.fa'''
    sequence_header_revcomp:        str             = f'''> {entry_title} sequence of {work_sequence_class.dna_length}bp:\n'''

    create_fasta_multiple_sequence_file_from_html(sequence_file_name_revcomp, sequence_header_revcomp, sequence_html_output_revcomp)
    ##########################################################

    return sequence_html_output_revcomp


def render_rna_amacid_prot_html_template(input_dna_sequence: str, work_sequence: str, entry_title: str):

    sequence_class:         DNASequence     = DNASequence(input_dna_sequence)
    work_sequence_rows:     str             = create_60_digits_format(work_sequence)

    second_header:          str             = f'''> {entry_title} sequence with length of {len(work_sequence)}:'''
    headers_list:           list[str]       = ['> Entered sequence:',  second_header]
    sequences_rows:         list[list[str]] = []

    sequences_rows.append(sequence_class.dna_60digits_sequence.split('\n'))
    sequences_rows.append(work_sequence_rows.split('\n'))

    output_data                             = zip(headers_list, sequences_rows)

    sequence_html_output:   str             = render_template('get_post_rna_amacid_protein.html',
                                                                        title  = entry_title,
                                                                        data   = output_data,)
                
    ## GENERATING FASTA ######################################                    
    sequence_file_name:     str             = f'''{entry_title}_sequence.fa'''
    sequence_header:        str             = f'''> {entry_title} sequence of {len(work_sequence)}bp:\n'''

    create_fasta_multiple_sequence_file_from_html(sequence_file_name, sequence_header, sequence_html_output)
    ##########################################################

    return sequence_html_output

#######################################################################
#----------------------------------------------------------------------
# AUTOMATING THE FILE RESULT FROM qPCR
#----------------------------------------------------------------------
#----------------------------------------------------------------------
# Automating Function 1:

def read_file_and_find_concentrations(file_path: Path) -> list[str]:
    
    file_string:            str             = file_path.read_text()
    data_rows:              list[str]       = file_string.split('\n')
    file_info:              str             = data_rows[0]
    headers:                list[str]       = data_rows[1].split('\t')
    rows_data:              list[list]      = [row.strip().split('\t') for row in data_rows[2:]]
    concentration_values:   list[str]       = [row_file[4] for row_file in rows_data if (row_file != [''])]

    return concentration_values

#----------------------------------------------------------------------
# Automating Function 2:

def calculate_samples_average(concentration_values_list: list[str]) -> list[float]:

    ############################################
    # FOR THE FUTURE THAT ONLY WORK FOR:
    #       DUPLICATE SAMPLES

    # if duplates:

    single_sample_average:  list[float]     = []
    samples_amount:         float           = (len(concentration_values_list)/2)
    duplicates_start:       int             = 0
    duplicates_end:         int             = 1
    for _ in range(int(samples_amount)):

        first_duplicate:    float           = float(concentration_values_list[duplicates_start])
        second_duplicate:   float           = float(concentration_values_list[duplicates_end])
        values:             list[float]     = []

        values.append(first_duplicate)
        values.append(second_duplicate)

        duplicates_average: float           = np.average(values)

        single_sample_average.append(duplicates_average)

        duplicates_start    = duplicates_start + 2
        duplicates_end      = duplicates_end + 2
    
    return single_sample_average

#----------------------------------------------------------------------
# Automating Function 3:

def calculate_exponential_value(single_sample_values: list[float], single_blank_values: list[float]):

    sample_blank_tuple                      = zip(single_sample_values, single_blank_values)
    blank_substraction_values:  list[float] = [(sample_value-blank_value) for sample_value,blank_value in sample_blank_tuple]
    final_values:               list[float] = [pow(2, value) for value in blank_substraction_values]

    return final_values

#----------------------------------------------------------------------
# Automating Function 4:

def create_bar_plot(plot_values: list[float]):

    plt.figure(facecolor='aliceblue', figsize=(12,6))
    plt.rcParams.update({'font.size': 12})
    kew_values: list = ['SMP-1', 'SMP-2', 'SMP-3', 'SMP-4', 'SMP-5', 'SMP-6']

    plt.bar(kew_values,plot_values, width=0.4, color='darkblue', edgecolor='white')
    plt.ylabel('Relative mRNA Expression')
    plt.xlabel('Samples')
    plt.xticks(rotation=20)
    plt.savefig('./static/my_plot.png')

#----------------------------------------------------------------------
# Automating Function 5:

def manage_qpcr_files(sample_file_name: str, blank_file_name: str):

    sample_path_name :              str            = f'''./uploads/{sample_file_name}'''
    sample_file_path :              Path            = Path(sample_path_name)
    sample_concentration_values:    list[str]       = read_file_and_find_concentrations(sample_file_path)
    single_sample_average:          list[float]     = calculate_samples_average(sample_concentration_values)

    blank_path_name :              str            = f'''./uploads/{blank_file_name}'''
    blank_file_path :               Path            = Path(blank_path_name)
    blank_concentration_values:     list[str]       = read_file_and_find_concentrations(blank_file_path)
    single_blank_average:           list[float]     = calculate_samples_average(blank_concentration_values)

    final_sample_values :           list[float]     = calculate_exponential_value(single_sample_average, single_blank_average)

    create_bar_plot(final_sample_values)


#######################################################################
#----------------------------------------------------------------------
# EXCEPTION FUNCTIONS FOR MANGE THE ERRORS
#----------------------------------------------------------------------

def create_dna_exception(input_dna_sequence: str, exception_title: str):

    if len(input_dna_sequence) > 0:
        validation_result:      str             = dna_validator(input_dna_sequence)
        message_error:          str             = '''> Something was wrong in your entered DNA sequence.'''
        exception_html_output:  str             = render_template('manage_post_errors.html',
                                                                title  = exception_title,
                                                                validation_result   = validation_result,
                                                                message_error       = message_error)
        return exception_html_output

    if input_dna_sequence == '':
        
        validation_result_empty:     str             = 'The validation was not possible.\nTry to write a sequence first.'
        exception_title_mod:         str             = f'''{exception_title} empty'''
        message_error_empty:         str             = '''> Any sequence was entered'''
        empty_exception_html_output: str             = render_template('manage_post_errors.html',
                                                                title  = exception_title_mod,
                                                                validation_result   = validation_result_empty,
                                                                message_error       = message_error_empty)
        return empty_exception_html_output
