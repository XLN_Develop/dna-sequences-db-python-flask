#! /usr/bin/env python3



# # -------------------------------------------------------------------
# # IMPORTS
# # -------------------------------------------------------------------
########### TRYING MATPLOTLIB #############

import matplotlib.pyplot as plt

# # -------------------------------------------------------------------
# # FUNCTIONS
# # -------------------------------------------------------------------

def create_bar_plot(plot_values: list[float]):

    plt.figure(facecolor='aliceblue', figsize=(12,6))
    plt.rcParams.update({'font.size': 12})
    label_values: list = ['SMP-1', 'SMP-2', 'SMP-3', 'SMP-4', 'SMP-5', 'SMP-6']

    plt.bar(label_values,plot_values, width=0.4, color='darkblue', edgecolor='white')
    plt.ylabel('Relative mRNA Expression')
    plt.xlabel('Samples')
    plt.xticks(rotation=20)
    #plt.savefig('./static/my_plot.png')

    plt.show()

def create_scatter_plot(plot_values: list[float]):

    plt.figure(facecolor='aliceblue', figsize=(12,6))
    plt.rcParams.update({'font.size': 12})
    label_values: list = ['SMP-1', 'SMP-2', 'SMP-3', 'SMP-4', 'SMP-5', 'SMP-6']

    plt.scatter(label_values,plot_values, color='darkblue', edgecolor='white')
    plt.ylabel('Relative mRNA Expression')
    plt.xlabel('Samples')
    plt.xticks(rotation=20)
    #plt.savefig('./static/my_plot.png')

    plt.show()

def create_plot_plot(plot_values: list[float]):

    plt.figure(facecolor='aliceblue', figsize=(12,6))
    plt.rcParams.update({'font.size': 12})
    label_values: list = ['SMP-1', 'SMP-2', 'SMP-3', 'SMP-4', 'SMP-5', 'SMP-6']

    plt.plot(label_values,plot_values, color='darkblue')
    plt.ylabel('Relative mRNA Expression')
    plt.xlabel('Samples')
    plt.xticks(rotation=20)
    #plt.savefig('./static/my_plot.png')

    plt.show()

def create_combined3_plot_plot(plot_values1: list[float], plot_values2: list[float], plot_values3: list[float]):

    plt.figure(facecolor='aliceblue', figsize=(12,6))
    plt.rcParams.update({'font.size': 12})
    label_values: list = ['SMP-1', 'SMP-2', 'SMP-3', 'SMP-4', 'SMP-5', 'SMP-6']

    plt.plot(label_values,plot_values1, color='darkblue', label='val1')
    plt.plot(label_values,plot_values2, color='green', label='val2')
    plt.plot(label_values,plot_values3, color='yellow', label='val3')
    
    plt.ylabel('Relative mRNA Expression')
    plt.xlabel('Samples')
    plt.xticks(rotation=20)
    #plt.savefig('./static/my_plot.png')
    plt.legend()
    plt.show()











##########3# TESTING PLOTS

val1: list = [0.016746460352129598, 0.014578640492762602, 0.015625, 0.0078125, 0.011048543456039806, 0.009290680585958758]
val2: list = [0.0078125, 0.009290680585958758, 0.016746460352129598, 0.0078125, 0.014578640492762602, 0.009290680585958758]
val3: list = [0.016746460352129598, 0.016746460352129598, 0.016746460352129598, 0.0078125, 0.0078125, 0.009290680585958758]
#create_bar_plot(val1)
# create_scatter_plot(val1)
# create_plot_plot(val1)
create_combined3_plot_plot(val1, val2, val3)
