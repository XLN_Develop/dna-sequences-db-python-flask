#! /usr/bin/env python3

''''
Here there are all functions used in the app_main.py
'''

#----------------------------------------------------------------------
# Imports
#----------------------------------------------------------------------

import  random

#######################################################################
#----------------------------------------------------------------------
# CLASS SeqReader
#----------------------------------------------------------------------
# Class SeqReader ------------------------------------------------

class SeqReader:
    
    #------------
    # Function 1:
    #------------
    def __init__(self, entry_sequence: str):

        # Precondition:
        assert (entry_sequence != ''), "No sequence entered"
        
        # Information extracted from the entry sequence:
        self.initial_sequence:      str = entry_sequence.upper()
        self.raw_sequence:          str = self.read_sequence(self.initial_sequence)
        self.length:                int = len(self.raw_sequence)
        self.to_60digits_sequence:  str = self.create_60_digits_format(self.raw_sequence)

        # For the functions that makes the object iterable:
        self.data:                  str = self.__str__()
        self.index:                 int = -1

    #------------
    # Function 2:
    #------------
    def remove_nonalpha_chars(self, random_data:str):

        no_break_lines_in_random_data : str = random_data.replace('\n', '')
        no_rows__in_random_data :       str = no_break_lines_in_random_data.replace('\r', '')
        no_tabs_in_random_data :        str = no_rows__in_random_data.replace('\t', '')
        
        alphabetic_data_string = ''        
        
        for digit in no_tabs_in_random_data:

            digit_is_alpha: bool = digit.isalpha()

            if not digit_is_alpha:
                pass
            
            if digit_is_alpha:

                alphabetic_data_string = alphabetic_data_string + digit
    
        return alphabetic_data_string

    #------------
    # Function 3:
    #------------
    def validate_sequence(self, nucleotide_sequence:str):

        checker = 'acdefghiklmnpqrstvwyACDEFGHIKLMNPQRSTVWY-*'
        count   = 0
        
        for nucleotide in nucleotide_sequence:

            if nucleotide in checker:
                pass
            if nucleotide not in checker:
                count = 1
                
        assert count == 0, 'Not a valid sequence'
        
        return nucleotide_sequence.upper()
    
    #------------
    # Function 5:
    #------------
    def read_sequence(self, entry_data: str):
        
        alpha_sequence = self.remove_nonalpha_chars(entry_data)
        valid_dna_seq  = self.validate_sequence(alpha_sequence)

        return valid_dna_seq

    #------------
    # Function 6:
    #------------
    def create_60_digits_format(self, entry_sequence: str) -> str:
        '''Manipulates only a entry sequence of, doesn't matter what,
        dona, proteins, etc....and return only the entry sequence, but
        with until 60 digits for each line.'''

        entry_sequence_list: list[str] = list(entry_sequence.replace(' ', ''))

        body:            str        = ''        
        count:           int        = 0
        nucelotide:      str

        for nucleotide in entry_sequence_list:
                body  = body  + nucleotide
                count = count + 1

                if count == 60 and not body.endswith('\n'):
                        body  = body  + '\n'
                        count = 0

        return body

    #-------------
    # Function 7:
    #-------------
    def __str__(self):

        dna_sequence_copy: str = self.raw_sequence
        return dna_sequence_copy
    
    #-------------
    # Function 8:
    #-------------
    def __iter__(self):

        return self

    #-------------
    # Function 9:
    #-------------
    def __next__(self):
        if self.index == (self.length -1):
            raise StopIteration
        self.index = self.index + 1
        return self.data[self.index]


#######################################################################
#----------------------------------------------------------------------
# CLASS PROTSequence
#----------------------------------------------------------------------
# Class PROTSequence ------------------------------------------------

class PROTSequence:

    one_to_three_dict:              dict = {'A':'Ala', 'R':'Arg', 'N':'Asn', 'D':'Asp',
                                            'C':'Cys', 'Q':'Gln', 'E':'Glu', 'G':'Gly',
                                            'H':'His', 'I':'Ile', 'L':'Leu', 'K':'Lys',
                                            'M':'Met', 'F':'Phe', 'P':'Pro', 'S':'Ser',
                                            'T':'Thr', 'W':'Trp', 'Y':'Tyr', 'V':'Val',
                                            '*':'End', '^':'Any'}

    protein_molecular_weight_dict:  dict = {'A':89.1, 'R':174.2,'N':132.1,'D':133.1,
                                            'C':121.2,'E':147.1,'Q':146.2,'G':75.1,
                                            'H':155.2,'I':131.2,'L':131.2,'K':146.2,
                                            'M':149.2,'F':165.2,'P':115.1,'S':105.1,
                                            'T':119.1,'W':204.2,'Y':181.2,'V':117.1,
                                            '*':0,    '^':136.905}

    hydropathy_dict:                dict= { 'A':1.8,   'R':-4.5,   'N':-3.5,   'D':-3.5,
                                            'C':2.5,   'Q':-3.5,   'E':-3.5,   'G':-0.4,
                                            'H':-3.2,  'I':4.5,    'L':3.8,    'K':-3.9,
                                            'M':1.9,   'F':2.8,    'P':-1.6,   'S':-0.8,
                                            'T':-0.7,  'W':-0.9,   'Y':-1.3,   'V':4.2,
                                            '*':0.0,   '^':-0.49}
    
    #------------
    # Function 1:
    #------------
    def __init__(self, entry_sequence: str):

        # Precondition:
        assert (entry_sequence != ''), "No sequence entered"
        
        # Information extracted from the entry sequence:
        self.prot_initial_sequence:      str        = entry_sequence.upper()
        self.prot_raw_sequence:          str        = self.read_sequence(self.prot_initial_sequence)
        self.prot_length:                int        = len(self.prot_raw_sequence)
        self.to_60digits_sequence:       str        = self.create_60_digits_format(self.prot_raw_sequence)
        self.prot_mol_weight_kDa:        float      = self.calculate_molecular_weight()
        self.prot_gravy:                 float      = self.calculate_protein_gravy()
        self.prot_to_amacid:             str        = self.translate_to_amacid()

        # For the functions that makes the object iterable:
        self.data:                      str         = self.__str__()
        self.index:                     int         = -1

    #------------
    # Function 2:
    #------------
    def remove_nonalpha_chars(self, random_data:str):

        no_break_lines_in_random_data : str = random_data.replace('\n', '')
        no_rows__in_random_data :       str = no_break_lines_in_random_data.replace('\r', '')
        no_tabs_in_random_data :        str = no_rows__in_random_data.replace('\t', '')
        
        alphabetic_data_string = ''        
        
        for digit in no_tabs_in_random_data:

            digit_is_alpha: bool = digit.isalpha()

            if not digit_is_alpha:
                pass
            
            if digit_is_alpha:

                alphabetic_data_string = alphabetic_data_string + digit
    
        return alphabetic_data_string

    #------------
    # Function 3:
    #------------
    def validate_protein_sequence(self, nucleotide_sequence:str):

        checker:    str = 'acdefghiklmnpqrstvwyACDEFGHIKLMNPQRSTVWY*^'
        count:      int = 0
        
        for nucleotide in nucleotide_sequence:

            if nucleotide in checker:
                pass
            if nucleotide not in checker:
                count = 1
                
        assert count == 0, 'Not a valid sequence'
        
        return nucleotide_sequence.upper()
    
    #------------
    # Function 5:
    #------------
    def read_sequence(self, entry_data: str):
        
        alpha_sequence = self.remove_nonalpha_chars(entry_data)
        valid_dna_seq  = self.validate_protein_sequence(alpha_sequence)

        return valid_dna_seq

    #------------
    # Function 6:
    #------------
    def create_60_digits_format(self, entry_sequence: str) -> str:
        '''Manipulates only a entry sequence of, doesn't matter what,
        dona, proteins, etc....and return only the entry sequence, but
        with until 60 digits for each line.'''

        entry_sequence_list: list[str] = list(entry_sequence.replace(' ', ''))

        body:            str        = ''        
        count:           int        = 0
        nucelotide:      str

        for nucleotide in entry_sequence_list:
                body  = body  + nucleotide
                count = count + 1

                if count == 60 and not body.endswith('\n'):
                        body  = body  + '\n'
                        count = 0

        return body

    #-------------
    # Function 7:
    #-------------
    def calculate_protein_gravy(self) -> float:
        '''Protein GRAVY returns the GRAVY (grand average of hydropathy)
        value for the protein sequences you enter'''

        protein_sequence:       str     = self.prot_raw_sequence
        entry_sequence_length:  int     = self.prot_length

        hydropathy_value: float = 0.0    

        for aminoacid in protein_sequence:
            hydropathy_value = hydropathy_value + (self.hydropathy_dict[aminoacid])
        
        gravy_value: float = round((hydropathy_value/entry_sequence_length), 4)

        return gravy_value

    #-------------
    # Function 8:
    #-------------
    def calculate_molecular_weight(self) -> float:

        

        mol_weight_value: float = 0.0    

        for aminoacid in self.prot_raw_sequence:

            mol_weight_value = mol_weight_value + (self.protein_molecular_weight_dict[aminoacid])

        molecular_weight_kDa: float = round((mol_weight_value/1000),2)

        return molecular_weight_kDa

    #-------------
    # Function 8:
    #-------------
    def translate_to_amacid(self) -> str:

        amacid_sequence_list: list[str] = [self.one_to_three_dict[amacid] for amacid in self.prot_raw_sequence]
        amacid_sequence:        str     = ''.join(amacid_sequence_list)

        return amacid_sequence

    #-------------
    # Function 10:
    #-------------
    def __str__(self):

        dna_sequence_copy: str = self.prot_raw_sequence
        return dna_sequence_copy
    
    #-------------
    # Function 11:
    #-------------
    def __iter__(self):

        return self

    #-------------
    # Function 12:
    #-------------
    def __next__(self):
        if self.index == (self.prot_length -1):
            raise StopIteration
        self.index = self.index + 1
        return self.data[self.index]

#######################################################################
#----------------------------------------------------------------------
# CLASS DNASequence
#----------------------------------------------------------------------
# Class DNASequence ---------------------------------------------------

class DNASequence:
    
    #--------------------
    # Class Attribute:
    #--------------------
    type:                           str  = 'DNA'
    codon_dictionary:               dict = {"UUU":"Phe", "UUC":"Phe", "UUA":"Leu", "UUG":"Leu",
                                            "CUU":"Leu", "CUC":"Leu", "CUA":"Leu", "CUG":"Leu",
                                            "AUU":"Ile", "AUC":"Ile", "AUA":"Ile", "AUG":"Met",
                                            "GUU":"Val", "GUC":"Val", "GUA":"Val", "GUG":"Val", 
                                            "UCU":"Ser", "UCC":"Ser", "UCA":"Ser", "UCG":"Ser", 
                                            "CCU":"Pro", "CCC":"Pro", "CCA":"Pro", "CCG":"Pro",
                                            "ACU":"Thr", "ACC":"Thr", "ACA":"Thr", "ACG":"Thr",
                                            "GCU":"Ala", "GCC":"Ala", "GCA":"Ala", "GCG":"Ala",
                                            "UAU":"Tyr", "UAC":"Tyr", "UAA":"End", "UAG":"End",
                                            "CAU":"His", "CAC":"His", "CAA":"Gln", "CAG":"Gln",
                                            "AAU":"Asn", "AAC":"Asn", "AAA":"Lys", "AAG":"Lys",
                                            "GAU":"Asp", "GAC":"Asp", "GAA":"Glu", "GAG":"Glu",                       
                                            "UGU":"Cys", "UGC":"Cys", "UGA":"End", "UGG":"Trp",
                                            "CGU":"Arg", "CGC":"Arg", "CGA":"Arg", "CGG":"Arg", 
                                            "AGU":"Ser", "AGC":"Ser", "AGA":"Arg", "AGG":"Arg", 
                                            "GGU":"Gly", "GGC":"Gly", "GGA":"Gly", "GGG":"Gly",
                                            "NNN":"ANY"}
    aminoacids_dict:                dict = {'Ala':'A', 'Arg':'R', 'Asn':'N', 'Asp':'D',
                                            'Cys':'C', 'Gln':'Q', 'Glu':'E', 'Gly':'G',
                                            'His':'H', 'Ile':'I', 'Leu':'L', 'Lys':'K',
                                            'Met':'M', 'Phe':'F', 'Pro':'P', 'Ser':'S',
                                            'Thr':'T', 'Trp':'W', 'Tyr':'Y', 'Val':'V',
                                            'End':'*', 'ANY':'^', 'Any':'^'}
    codon_predictor_dictionary:     dict = {}

    protein_molecular_weight_dict:  dict = {'A':89.1, 'R':174.2,'N':132.1,'D':133.1,
                                            'C':121.2,'E':147.1,'Q':146.2,'G':75.1,
                                            'H':155.2,'I':131.2,'L':131.2,'K':146.2,
                                            'M':149.2,'F':165.2,'P':115.1,'S':105.1,
                                            'T':119.1,'W':204.2,'Y':181.2,'V':117.1,
                                            '*':0,    '^':136.905}


    #------------
    # Function 1:
    #------------
    def __init__(self, entry_sequence: str):

        # Precondition:
        assert (entry_sequence != ''), "No sequence entered"
        
        # Information extracted from the entry sequence:
        self.dna_initial_sequence:                          str     = entry_sequence.upper()
        self.dna_raw_sequence:                              str     = self.read_sequence(self.dna_initial_sequence)
        self.dna_length:                                    int     = len(self.dna_raw_sequence)
        self.dna_60digits_sequence:                         str     = self.create_60_digits_format(self.dna_raw_sequence)
        self.a,self.t,self.c,self.g                                 = self.calculate_nucleotides_amount()
        self.a_perc,self.t_perc,self.c_perc,self.g_perc             = self.calculate_nucleotides_percent()
        self.dna_melting_temperature:                       float   = self.calculate_fusion_temperature()
        self.dna_codons_amount:                             str     = self.calculate_adn_codons()
        self.dna_reverse:                                   str     = self.create_reverse_seq()
        self.dna_complement:                                str     = self.create_complement_seq(self.dna_raw_sequence)
        self.dna_reverse_complement:                        str     = self.create_reverse_complement_seq()
        self.dna_to_rna:                                    str     = self.change_thymine_to_uracil(self.dna_raw_sequence)
        self.dna_to_aminoacid:                              str     = self.translate_dna_to_aminoacid()
        self.dna_to_protein:                                str     = self.translate_dna_to_one_letter_aminoacid()
        self.dna_protein_mol_weight_kDa:                    float   = self.calculate_molecular_weight()
        self.dna_protein_gravy:                             float   = self.calculate_protein_gravy()        
        self.dna_sequence_status:                           str     = self.dna_status()

        # For the functions that makes the object iterable:
        self.data:                                          str     = self.__str__()
        self.index:                                         int     = -1
    
    #------------
    # Function 2:
    #------------
    def remove_nonalpha_chars(self, random_data:str):

        no_break_lines_in_random_data : str = random_data.replace('\n', '')
        no_rows__in_random_data :       str = no_break_lines_in_random_data.replace('\r', '')
        no_tabs_in_random_data :        str = no_rows__in_random_data.replace('\t', '')
        no_spaces_in_random_data :        str = no_tabs_in_random_data.replace(' ', '')
        
        alphabetic_data_string = ''        
        
        for digit in no_spaces_in_random_data:

            # digit_is_alpha: bool = digit.isalpha()
            
            # if not digit_is_alpha:
            #     pass
            
            # if digit_is_alpha:
            alphabetic_data_string = alphabetic_data_string + digit
    
        return alphabetic_data_string
    
    #------------
    # Function 3:
    #------------
    def validate_dna_sequence(self, nucleotide_sequence:str):

        checker = 'atcgrynATCGRYN'
        count   = 0
        
        for nucleotide in nucleotide_sequence:

            if nucleotide in checker:
                pass
            if nucleotide not in checker:
                count = 1
                
        assert count == 0, 'Not a valid DNA sequence'
        
        return nucleotide_sequence.upper()

    #------------
    # Function 4:
    #------------
    def validate_protein_sequence(self, nucleotide_sequence:str):

        checker = 'acdefghiklmnpqrstvwyACDEFGHIKLMNPQRSTVWY*^'
        count   = 0
        
        for nucleotide in nucleotide_sequence:

            if nucleotide in checker:
                pass
            if nucleotide not in checker:
                count = 1
                
        assert count == 0, 'Not a valid Protein sequence'
        
        return nucleotide_sequence.upper()
    
    #------------
    # Function 5:
    #------------
    def read_sequence(self, entry_data: str):
        
        alpha_sequence = self.remove_nonalpha_chars(entry_data)
        valid_dna_seq  = self.validate_dna_sequence(alpha_sequence)

        return valid_dna_seq

    #------------
    # Function 6:
    #------------
    def create_60_digits_format(self, entry_sequence: str) -> str:
        '''Manipulates only a entry sequence of, doesn't matter what,
        dona, proteins, etc....and return only the entry sequence, but
        with until 60 digits for each line.'''

        entry_sequence_list: list[str] = list(entry_sequence.replace(' ', ''))

        body:            str        = ''        
        count:           int        = 0
        nucelotide:      str

        for nucleotide in entry_sequence_list:
                body  = body  + nucleotide
                count = count + 1

                if count == 60 and not body.endswith('\n'):
                        body  = body  + '\n'
                        count = 0

        return body

    #------------
    # Function 7.1:
    #------------
    def calculate_nucleotides_amount(self) -> tuple:

        sequence: str = self.dna_raw_sequence
        total_nucleotides_amount: int = len(sequence)

        nucleotides: list[str] = ['A','T','C','G']
        a_result:    str = ''
        t_result:    str = ''
        c_result:    str = ''
        g_result:    str = '' 

        for nucleotide in nucleotides:

            nucleotide_amount: int = sequence.count(nucleotide)

            if nucleotide == 'A':
                a_amount: int  =  nucleotide_amount
                a_result       = str(a_amount)

            if nucleotide == 'T':
                t_amount: int  =  nucleotide_amount
                t_result       = str(t_amount)

            if nucleotide == 'C':
                c_amount: int  =  nucleotide_amount
                c_result       = str(c_amount)

            if nucleotide == 'G':
                g_amount: int  =  nucleotide_amount
                g_result       = str(g_amount)

        return a_result,t_result,c_result,g_result

    #------------
    # Function 7.2:
    #------------
    def calculate_nucleotides_percent(self) -> tuple:

        sequence:                   str = self.dna_raw_sequence
        total_nucleotides_amount:   int = len(sequence)

        # nucleotides: list[str] = ['A','T','C','G']
        a_perc:   str = str(round((int(self.a)/total_nucleotides_amount)*100, 2))
        t_perc:   str = str(round((int(self.t)/total_nucleotides_amount)*100, 2))
        c_perc:   str = str(round((int(self.c)/total_nucleotides_amount)*100, 2))
        g_perc:   str = str(round((int(self.g)/total_nucleotides_amount)*100, 2))

        return a_perc,t_perc,c_perc,g_perc

    #------------
    # Function 8:
    #------------
    def calculate_fusion_temperature(self) -> float:
        '''Tm oligonucleòtids = 2(A+T) + 4(G+C)'''

        a_amount:   int     = self.dna_raw_sequence.count('A')
        t_amount:   int     = self.dna_raw_sequence.count('T')
        c_amount:   int     = self.dna_raw_sequence.count('C')
        g_amount:   int     = self.dna_raw_sequence.count('G')
        tm_result:  float   = (2*(a_amount + t_amount))+(4*(g_amount + c_amount))

        return tm_result

    #------------
    # Function 9:
    #------------
    def make_triplets(self, entry_rna_sequence: str) -> list[str]:
        '''Recieve a squence and split it in slices of 3 letters'''    
        
        entry_sequence_list:    list[str] = list(entry_rna_sequence)
        copy_sequence_list:     list[str]   = entry_sequence_list
        triplet_string:         str         = ''
        sequence_triplets_list: list[str]   = []

        for nuclotide in copy_sequence_list:

            triplet_string       = triplet_string + (nuclotide)
            is_triplet:     bool = len(triplet_string) == 3

            if is_triplet:

                sequence_triplets_list.append(triplet_string)
                triplet_string = ''
        
        return sequence_triplets_list

    #------------
    # Function 10:
    #------------
    def calculate_adn_codons(self) -> str:

        sequence_triplets_list:     list[str]   = self.make_triplets(self.dna_raw_sequence)
        protein_sequence_length:    int         = len(sequence_triplets_list)
        codons_found:               list[str]   = []
        codons_times_found:         list[int]   = []
        codons_percents:            list[float] = []

        for triplet in sequence_triplets_list:

            if triplet in codons_found:
                pass

            if triplet not in codons_found:

                codons_found.append(triplet)

                codon_count:    int     = sequence_triplets_list.count(triplet)
                codons_times_found.append(codon_count)

                codon_percent:  float   = round((codon_count*100)/protein_sequence_length, 2)
                codons_percents.append(codon_percent)

        codons_information:     list[tuple] = sorted(list(zip(codons_found, codons_times_found, codons_percents)))
        result:                 str         = '> Codon analysis:\nCodon__Times Found__Percent\n'

        for codon_tuple in codons_information:

            codon_result: str   = f'{codon_tuple[0]}_____{codon_tuple[1]}_____________{codon_tuple[2]}%  \n'
            result              = result + codon_result
        
        return result
    
    #-------------
    # Function 11:
    #-------------
    def create_reverse_seq(self) -> str:
        '''This function recieve an entry fasta or entry sequence of nucleotides,
        and returns as a fasta structure, the same sequence but reverse'''

        entry_sequence_list:    list[str]   = list(self.dna_raw_sequence)
        reverse_sequence_list:  list        = list(reversed(entry_sequence_list))
        reverse_seq:            str         = ''.join(reverse_sequence_list)
        reverse_seq_fa:         str         = self.create_60_digits_format(reverse_seq)

        return reverse_seq_fa

    #-------------
    # Function 12:
    #-------------
    def create_complement_seq(self, entry_sequence: str) -> str:
        '''This function, creates the comlementary sequence of self.dna_row_sequence
        , and can recieve an an entry fasta or entry sequence of nucleotides from the
        reverse sequence and returns its complementary sequence'''


        entry_seq_string:           str         = self.read_sequence(entry_sequence)
        complement_nucleotide_dict: dict        = { 'A':'T',
                                                    'C':'G',
                                                    'T':'A',
                                                    'G':'C',
                                                    'N':'N',
                                                    'R':random.choice(['G','A']),
                                                    'Y':random.choice(['C','T'])}
        
        entry_sequence_list:        list[str]   = list(entry_seq_string)
        complement_seq_list:        list[str]   = [complement_nucleotide_dict[nucleotide] 
                                                    for nucleotide in entry_sequence_list]

        complement_sequence:        str         = ''.join(complement_seq_list)
        complement_sequence_fa:     str         = self.create_60_digits_format(complement_sequence)

        return complement_sequence_fa

    #-------------
    # Function 13:
    #-------------
    def create_reverse_complement_seq(self) -> str:
        '''This function recieve an entry fasta or entry sequence of nucleotides, and returns 
        as a fasta structure, its reverse and complementary sequence'''

        reverse_seq_fa:            str = self.read_sequence(self.dna_reverse)
        rev_compl_seq_fa:          str = self.create_complement_seq(reverse_seq_fa)

        return rev_compl_seq_fa    

    #-------------
    # Function 14:
    #-------------
    def change_thymine_to_uracil(self, entry_dna_sequence: str) -> str:
        '''This function recieve an entry sequence or entry fasta of nucleotides,
        and change all the T (thymine) for U (uracil)'''
        
        # Precondition:
        assert (entry_dna_sequence != ''), "No sequence to change T for U. Your entry data is empty!"

        rna_sequence: str = entry_dna_sequence.replace('T', 'U')
        
        # Postcondition:
        assert (rna_sequence != '') and (rna_sequence.count('T') == 0), "There's NO sequence to return or, still 'T' in sequence product"

        return rna_sequence
    
    #-------------
    # Function 15:
    #-------------
    def find_pattern_list_indexes(self, entry_list: list[str], pattern: str) -> list[int]:
        '''Search for pattern indexes. Takes each string from the entry
        list, and search if the patttern is in. If it is, find the index
        from the string into the entry list and store it into a new list. 
        Return a list of the indexes (integers)'''

        entry_list_copy:    list[str]   = entry_list.copy()
        indexes_list:       list[int]   = []
        replacement:        str         = '---'

        for string in entry_list_copy:
            pattern_in:     bool        = pattern in string

            if pattern_in:

                string_index: int = entry_list_copy.index(string)
                indexes_list.append(string_index)
                entry_list_copy.remove(string)
                entry_list_copy.insert(string_index, replacement)
            
            else:
                pass
        
        return indexes_list

    #-------------
    # Function 16:
    #-------------
    def extract_only_amacid_sequence_from_fasta(self, data_string: str):       
        
        # Setting up some useful variables for to be used in the conditions that will follow after:
        fasta_list:             list[str]       = data_string.split('\n')
        header:                 str             = fasta_list[0]
        body_list:              list[str]       = fasta_list[1:]
        body_string:            str             = (''.join(body_list)).replace(' ', '')
        posible_error:          str             = ''
        checker:                str             = 'AlaArgAsnAspCysGlnGluGlyHisIleLeuLysMetPheProSerThrTrpTyrValEndANYAny'

        # Setting up the condition(s) if entry data is in a FASTA structure. (CONDITION 2)
        is_fasta:               bool            = data_string.startswith('\n>') or data_string.startswith('>')
        
        # Setting up the condition(s) if entry data is in a FASTA structure but HAVE NO '\n' in. (CONDITION 1)
        line_break:             str             = '\n'
        line_break_not_in:      bool            = (line_break not in data_string)
        is_wierd_case:          bool            = is_fasta and line_break_not_in

        # Setting up the condition(s) if entry data in OLNY SEQUENCE. (CONDITION 3)
        modified_sequence:      str             = data_string.replace('\n', '')
        is_only_sequence:       bool            = modified_sequence.isalpha()

        # Condition 1: is FASTA and NO '\n' where found.
        if is_wierd_case:

                sequence_string_list_1:   list[str]       = [digit for digit in data_string if digit in checker]
                sequence_string_1:        str             = ''.join(sequence_string_list_1)
                
                return sequence_string_1

        # Condition 2: is FASTA and '\n' where found.
        if is_fasta:          

                pattern:            str     = '>'
                multiple_pattern:   bool    = data_string.count(pattern) > 1

                if multiple_pattern:

                    data_string_splited:        list[str]   = data_string.split(pattern)
                    fasta_sequence:             str         = data_string_splited[len(data_string_splited)-1]
                    fasta_sequence_splitted:    list[str]   = fasta_sequence.split('\n')
                    amacid_sequence:            str         = ''.join(fasta_sequence_splitted[1:])
                    return amacid_sequence

                if not multiple_pattern:

                    if header == posible_error:

                            fasta_list.remove(posible_error)
                            header       = fasta_list[0]
                            body_list    = fasta_list[1:]
                            body_string  = (''.join(body_list)).replace(' ', '')

                    sequence_string_list_2:   list[str]       = [digit for digit in body_string if digit.isalpha()]
                    sequence_string_2:        str             = ''.join(sequence_string_list_2)

                    return sequence_string_2

        # Condition 3: is OLNY SEQUENCE. NO FASTA.
        if is_only_sequence:
                
                return modified_sequence
        
        # Condition 4: else cases.
        else:
                
                sequence_string_list_3:   list[str]       = [digit for digit in data_string if digit in checker]
                sequence_string_3:        str             = ''.join(sequence_string_list_3)
                
                return sequence_string_3

    #-------------
    # Function 17:
    #-------------
    def codon_predictor(self, codons_to_predict: list[str]) -> dict:
        '''This function recieves an entry list of codons, make a copy
        of it and works with this copy'''
    
        codons_to_predict_copy:     list[str]   = codons_to_predict.copy()
        predictor_dictionary:       dict        = {}    
        substitutes_list:           list[str]   = ['A','U','G','C']
        target:                     str         = 'N'    

        # 1. Takes a CODON from codons_to_predict_list. All the list like this
        for codon in codons_to_predict_copy:
            codon_with_one_target_substituted_list: list[str] = []
            possibilities:                          list[str] = []

            # 2. Setting up if only one 'N' was fount into the CODON.
            codon_repeats:      int  = codon.count(target)
            single_repeat:      bool = (codon_repeats == 1)
            two_repeats:        bool = (codon_repeats == 2)
            three_repeats:      bool = (codon_repeats == 3)

            # 3. Execute next 'if' statement when 'single_repeat' is True.
            if single_repeat:

                # 3.1. Separate the three NUCLEOTIDES of the CODON and creates a list. Example: ['N','T','G']
                codon_as_list_1: list[str]  = list(codon)
                

                # 3.2. Store the index number into a variable to use it after.
                target_index_1: int         = codon_as_list_1.index(target)

                # 3.3. For each subtitute in 'substitutes_list'.
                #       - Remove the target 'N'.
                #       - Insert a substitute in the place of the 'N' removed.
                #       - Create a new CODON string with the replacement.
                #       - Append the new CODON strin into the list 'possibilities'.
                for substitute in substitutes_list:

                    codon_as_list_1.pop(target_index_1)
                    codon_as_list_1.insert(target_index_1, substitute)
                    codon_as_string_1: str = ''.join(codon_as_list_1)
                    possibilities.append(codon_as_string_1)

                    # 3.3.1. Store the possibilities for each codon I want to predict the translation
                    # into a dictionry inside this function.
                    if codon not in predictor_dictionary:

                        predictor_dictionary.update({codon:possibilities})

                    # 3.3.2. Store the possibilities for each codon I want to predict the translation
                    # into a dictionary in the beginning of the class, out from the functions,
                    # to try to store all the predictions when this function calculates it
                    # for all the sequences that went trougth this function.
                    if codon not in self.codon_predictor_dictionary:

                        self.codon_predictor_dictionary.update({codon:possibilities})
            
            # 4. Execute next 'if' statement when 'multiple_repeat' is True.
            if two_repeats:

                    # 4. Separate the three NUCLEOTIDES of the CODON and creates a list. Example: ['N','N','G']
                codon_as_list:  list[str] = list(codon)                

                        # 6. Store the index number into a variable to use it after.
                target_indexes: list[int] = self.find_pattern_list_indexes(codon_as_list, target)

                for substitute in substitutes_list:

                    codon_as_list.pop(target_indexes[0])
                    codon_as_list.insert(target_indexes[0], substitute)
                    codon_as_string: str = ''.join(codon_as_list)
                    codon_with_one_target_substituted_list.append(codon_as_string)

                for second_codon in codon_with_one_target_substituted_list:

                    final_codon_as_list:    list[str]   = list(second_codon)
                    target_index_2:         int         = final_codon_as_list.index(target)

                    for substitute in substitutes_list:

                        final_codon_as_list.pop(target_index_2)
                        final_codon_as_list.insert(target_index_2, substitute)
                        final_codon_as_string: str = ''.join(final_codon_as_list)
                        possibilities.append(final_codon_as_string)            

                if codon not in predictor_dictionary:

                    predictor_dictionary.update({codon:possibilities})

                if codon not in self.codon_predictor_dictionary:

                    self.codon_predictor_dictionary.update({codon:possibilities})

            if three_repeats:

                all_possibilities: list = ['NNN']
                predictor_dictionary.update({codon:all_possibilities})
                self.codon_predictor_dictionary.update({codon:all_possibilities})

            possibilities = []
            
        return predictor_dictionary

    #-------------
    # Function 18:
    #-------------
    def translate_dna_to_aminoacid(self) -> str:

        dna_row_sequence_without_r_and_y:   str         = self.dna_raw_sequence
        purine_options:                     list[str]   = ['G','A']
        pyrimidine_options:                 list[str]   = ['C','T']

        if 'R' in dna_row_sequence_without_r_and_y:

            sequence_list_1:    list[str] = list(dna_row_sequence_without_r_and_y)
            r_indexes_list:     list[int] = self.find_pattern_list_indexes(sequence_list_1, 'R')

            for index in r_indexes_list:

                sequence_list_1.pop(index)
                sequence_list_1.insert(index, random.choice(purine_options))

            dna_row_sequence_without_r_and_y = ''.join(sequence_list_1)

        if 'Y' in dna_row_sequence_without_r_and_y:

            sequence_list_2:    list[str] = list(dna_row_sequence_without_r_and_y)
            y_indexes_list:     list[int] = self.find_pattern_list_indexes(sequence_list_2, 'Y')

            for index in y_indexes_list:

                sequence_list_2.pop(index)
                sequence_list_2.insert(index, random.choice(pyrimidine_options))

            dna_row_sequence_without_r_and_y = ''.join(sequence_list_2)
                
        rna_sequence:       str       = self.change_thymine_to_uracil(dna_row_sequence_without_r_and_y)
        rna_triplets:       list[str] = self.make_triplets(rna_sequence)

        if 'N' in rna_sequence:            
            
            # Find the triplets of the codons with 'N' inside. CAN BE DUPLICATED.
            undefined_triplets_list:            list[str] = [triplet for triplet in rna_triplets if 'N' in triplet]  
            # Unduplicated list  of the codons with 'N' inside.
            undefined_triplets_unduplicated:    list[str] = list(set(undefined_triplets_list))
            # Dictionari of the possibilities for each codon with 'N'
            predicted_triplets_dictionary:      dict      = self.codon_predictor(undefined_triplets_unduplicated)                    
            # Makeing a copy of the entry rna_triplets, because after i will modify it:
            rna_triplets_copy:                  list[str] = rna_triplets.copy()

            # For FIRST HEADER:
            #------------------

            # Find the indexes of the codons with 'N' inside.
            undefined_triplets_indexes_list:    list[int] = self.find_pattern_list_indexes(rna_triplets, 'N')

            # To show in the output the triplets indexes. +1 to each index in undefined_triplets_indexes_list
            output_indexes_list:                list[int] = []

            for index in undefined_triplets_indexes_list:

                codon_key:          str         = rna_triplets[index]
                candidates:         list[str]   = predicted_triplets_dictionary[codon_key]
                random_candidate:   str         = random.choice(candidates)
                
                rna_triplets_copy.pop(index)
                rna_triplets_copy.insert(index, random_candidate)
                output_indexes_list.append(index+1)

            # For SECOND HEADER:
            #-------------------

            aminoacids_list_3:                list[str]   = [self.codon_dictionary[triplet] for triplet in rna_triplets_copy]
            aminoacids_raw_sequence_3:        str         = ''.join(aminoacids_list_3)
            aminoacids_formatted_sequence_3:  str         = self.create_60_digits_format(aminoacids_raw_sequence_3)
            predicted_triplets_string:        str         = ''

            for index in output_indexes_list:
                
                string_prediction:            str = f'Pos.{index}-{rna_triplets[index-1]}: {predicted_triplets_dictionary[rna_triplets[index-1]]}\n'
                predicted_triplets_string         = predicted_triplets_string + string_prediction
            
            # For THIRD HEADER:
            #------------------

            predicted_traductions_list:          list       = []

            for triplet in undefined_triplets_unduplicated:

                for value in predicted_triplets_dictionary[triplet]:

                    traduction:                 str = self.codon_dictionary[value]
                    triplet_traduction_string:  str = f'{value}: {traduction}\n'
                    predicted_traductions_list.append(triplet_traduction_string)

            predicted_traductions_string:       str = ''.join(sorted(set(predicted_traductions_list)))

            first_header:       str = f'''> When translating to AmAcid there was codons undefinied at next CODON positions (subtituted after randomly between predictions)\n{output_indexes_list}\n'''
            second_header:      str = f'''> Undefined triplets found with it's own possibilities available. If multiple predictions found, chose one randomly:\n{predicted_triplets_string}'''
            third_header:       str = f'''> Predictions translation:\n{predicted_traductions_string}'''
            last_header:        str = f'> DNA to AmAcid translation. Predicted sequence:\n{aminoacids_formatted_sequence_3}'
            result_3:           str = f'{first_header}\n{second_header}\n{third_header}\n{last_header}'

            return result_3  

        else:

            aminoacids_list_4:                list[str]   = [self.codon_dictionary[triplet] for triplet in rna_triplets]
            aminoacids_raw_sequence_4:        str         = ''.join(aminoacids_list_4)
            aminoacids_formatted_sequence_4:  str         = self.create_60_digits_format(aminoacids_raw_sequence_4)
            header:                           str         = '> DNA to AmAcid translation:\n'
            result_4:                         str         = f'{aminoacids_formatted_sequence_4}'
            return result_4      

    #-------------
    # Function 19:
    #-------------
    def translate_dna_to_one_letter_aminoacid(self) -> str:
        '''Recieve a entry sequene of aminoacids and replace each aminoacid for
            it's own single letter nomenclature'''
        
        assert (self.dna_to_aminoacid != '')

        amacid_sequence_extracted:          str        = self.extract_only_amacid_sequence_from_fasta(self.dna_to_aminoacid)
        raw_amacid_sequence:                str        = amacid_sequence_extracted.replace('\n', '')
        aminoacids_triplets:                list[str]  = self.make_triplets(raw_amacid_sequence)
        aminoacids_triplets_capitalized:    list[str]  = [triplet.capitalize() for triplet in aminoacids_triplets]
        translated_aa_list:                 list[str]  = [self.aminoacids_dict[triplet] for triplet in aminoacids_triplets_capitalized]
        translated_aa_sequence:             str        = ''.join(translated_aa_list)
        formattetd_protein_sequence:        str        = self.create_60_digits_format(translated_aa_sequence)
        header:                             str        = ''' ONE letter AmAcid translation. STOP codon represented by '-'. ANY possibility represented by '*':\n'''
        result:                             str        = f'{formattetd_protein_sequence}'

        return result

    #-------------
    # Function 20:
    #-------------
    def calculate_protein_gravy(self) -> float:
        '''Protein GRAVY returns the GRAVY (grand average of hydropathy)
        value for the protein sequences you enter'''

        protein_sequence:       str     = self.dna_to_protein
        checked_protein_seq:    str     = self.remove_nonalpha_chars(protein_sequence)
        valid_protein_seq:      str     = self.validate_protein_sequence(checked_protein_seq)
        entry_sequence_length:  int     = len(valid_protein_seq)
        hydropathy_dict:        dict    = { 'A':1.8,   'R':-4.5,   'N':-3.5,   'D':-3.5,
                                            'C':2.5,   'Q':-3.5,   'E':-3.5,   'G':-0.4,
                                            'H':-3.2,  'I':4.5,    'L':3.8,    'K':-3.9,
                                            'M':1.9,   'F':2.8,    'P':-1.6,   'S':-0.8,
                                            'T':-0.7,  'W':-0.9,   'Y':-1.3,   'V':4.2,
                                            '*':0.0,   '^':-0.49}

        hydropathy_value: float = 0.0    

        for aminoacid in valid_protein_seq:
            hydropathy_value = hydropathy_value + (hydropathy_dict[aminoacid])
        
        gravy_value: float = round((hydropathy_value/entry_sequence_length), 4)

        result: str = f'''> Protein GRAVY results:\nResults for {entry_sequence_length} residue sequence:\n{gravy_value}'''

        return gravy_value

    #-------------
    # Function 21:
    #-------------
    def calculate_molecular_weight(self) -> float:

        protein_sequence:       str     = self.dna_to_protein
        checked_protein_seq:    str     = self.remove_nonalpha_chars(protein_sequence)
        valid_protein_seq:      str     = self.validate_protein_sequence(checked_protein_seq)
        entry_sequence_length:  int     = len(valid_protein_seq)

        mol_weight_value: float = 0.0    

        for aminoacid in valid_protein_seq:

            mol_weight_value = mol_weight_value + (self.protein_molecular_weight_dict[aminoacid])

        molecular_weight_kDa: float = round((mol_weight_value/1000),2)

        return molecular_weight_kDa

    #-------------
    # Function 22:
    #-------------
    def dna_status(self):

        reverse:                str = f'''> Reverse sequence:\n{self.dna_reverse}\n'''
        complement:             str = f'''> Complement sequence:\n{self.dna_complement}\n'''
        rev_complement:         str = f'''> Reverse-Complement sequence:\n{self.dna_reverse_complement}\n'''
        entry_sequence:         str = f'''Entered sequence with length {self.dna_length}:\n{self.dna_60digits_sequence}\n'''
        dna_nucleotides_amount: str = f'''> Nucleotides amount:\nA: {self.a}\nT: {self.t}\nC: {self.c}\nG: {self.g}\n'''
        rev_com_result:         str = f'''{reverse}\n{complement}\n{rev_complement}\n'''
        amacid_traduction:      str = f'''> DNA to AmAcid translation:\n{self.dna_to_aminoacid}\n'''
        protein_traduction:     str = f'''> To Protein Traduction:\n{self.dna_to_protein}\n'''


        # Result soulfd include:

        #   1- Entered sequence
        #   2- Nucleotides amount.
        #   3- Melting Temperature Tm.
        #   3- Codons amount.
        #   4- Reverse sequence.
        #   5- Complement sequence.
        #   6- Reverse-Complement sequence.
        #   7- Translation to aminoacid.
        #   8- Translation to protein.
        #   9- Protein GRAVY

        result:         str = f'''{entry_sequence}\n{rev_com_result}\n{dna_nucleotides_amount}\n{self.dna_codons_amount}\n{amacid_traduction}\n{protein_traduction}'''

        return result

    #-------------
    # Function 23:
    #-------------
    def __str__(self):

        dna_sequence_copy: str = self.dna_raw_sequence
        return dna_sequence_copy
    
    #-------------
    # Function 24:
    #-------------
    def __iter__(self):

        return self

    #-------------
    # Function 25:
    #-------------
    def __next__(self):
        if self.index == (self.dna_length -1):
            raise StopIteration
        self.index = self.index + 1
        return self.data[self.index]

#----------------------------------------------------------------------

# if __name__ == '__main__':

