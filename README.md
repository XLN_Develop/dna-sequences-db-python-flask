
# Sequences & Data Base (S&DB)

## Projects Aim

That is a web application that mix a DNA Sequence Manipulation Tool with Data Base.

With this web application you can work either with random DNA sequences or with your own, see a detailed analysis of a sequence, and also store it easily in a Data Base Table that will show you some different information for each sequence inserted in, like:

- Sequence length.
- Nucleotides amount and percent.
- Translations.
- Melting Temperature.
- Molecular Weight.
- GRAVY.

Can also easily download your Data Base Table to your computer for a further analysis.

## Technologies and libraries

- Python 3.9.
- Jinja 3.0.x.
- HTML.
- CSS.
- flask library.
- sqlite3 library.

## Fields & Features

Here there are all the fields an features you can find in Sequences & Data Base (S&DB).

#### 1. DNA sequences

There is some different options for sequence maniplation. 

- Validate DNA.
- Filter & Validate DNA.
- Reverse & Complement.
- DNA Translate.
- DNA Range Extractor.
- Combine Sequences.
- Find Pattern.
- Nucleotides Amount.
- Codons Amount.
- DNA Molecular Weight.
- DNA to Protein GRAVY.

With DNA Translate can translate to RNA, amino acid or protein.
With Reverse & Complement can get the reverse, the complement, or the reverse and complement sequence.

#### 2. Protein sequences

There is some different options for sequence maniplation. 

- Validate Protein.
- Filter & Validate Protein.
- Protein Molecular Weight.
- Protein to Protein GRAVY.

#### 3. DNA sequences Data Base

- Create your DNA Data Base.

With the Create your DNA Data Base option, you can insert your sequences in a Data Base showed in a
table. There is the options to filter the table to show the desired sequences.

- Show table:

	- Filter Table Data.
	- Show DNA sequence analysis.
	- Download table data.

- Table columns:

	- __Sequence__: the entered DNA sequence.
	- __Length__: length of the entry sequence.
	- __A_Amount__: amount of Adenines.
	- __T_Amount__: amount of Thymines
	- __C_Amount__: amount of Cytosines
	- __G_Amount__: amount of Guanines
	- __Melting Temp TM__: melting temperature in °C.

		- ___Formula__: Tm = 2(A+T) + 4(G+C)_

	- __To Protein__: Protein traduction.
	- __Protein GRAVY__: Grand Average of Hydropathy
	- __A_Amount__: amount of Adenines.
	
#### 4. Random sequences

Generate random DNA sequences of different length.

- Random DNA seq.
- Random Protein seq.

#### 5. Experimental Tools (not implemented. Work in progress).

Generate random DNA sequences of different length.

- IPH Tool.

## Root Files & Directories

### Root Files:

	- app_main.py:
 
		- Main file of the project.
	
	- my_clases.py: 

		- Here there is the Class SeqReader and DNASequence that complements the main file (app_main.py), most of all with calculations.

	- my_utils.py: 

		- Here there is all the functions that complements the main file (app_main.py), most of all with calculations.

	- my_plots.py (not implemented. Work in progress): 

		- Here there is all the functions that complements the main file (app_main.py), for
		the not implemented field IPD Tool.

	- sql.py: 

		- Here there is all the functions that complements the main file (app_main.py), for the
		operations in Data Base like create the DB and the filter options.

	- README.md

### Root Directories:

	- static: 

		- All the CSS style files and images are in this directory.

	- templates: 

		- All the HTML Jinja template files are in this directory.

	- uploads (not implemented. Work in progress): 

		- For the not implemented field IPH Tool. This option allows to upload a file to analyze
		it's data and show a plot.

## Inspiration

The code is not inspired with any code, but, the idea of this web application Sequences & Data Base (S&DB) growth using SMS Suite (https://www.bioinformatics.org/sms2/), but adding the improve that you can store
the sequences easily in your Data Base and download it form there to a further analysis.