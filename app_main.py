#! /usr/bin/env python3

''''
That is a dinamic web application mixed with Data Base usinf Flask and sqlite3,
called Sequences & Data Base.
'''

# # -----------------------------------------------------------------------------
# # IMPORTS
# # -----------------------------------------------------------------------------

from    flask           import      Flask, Response, request, redirect
from    flask           import      render_template, send_file, flash
from    pathlib         import      Path
from    my_clases       import      DNASequence, SeqReader, PROTSequence
from    werkzeug.utils  import      secure_filename
import  markdown
import  sqlite3
import  my_utils
import  sql
import  os
import  pprint

# # -------------------------------------------------------------------
# # FLASK INIZIALIZATION
# # -------------------------------------------------------------------
# # This next 2 lines always have to be written.

module_name:    str     = __name__
app:            Flask   = Flask(module_name)

# # -------------------------------------------------------------------
# # PREPARING FOR THE UPLOADING FILES TO SERVER TO BE SERVED.
# # -------------------------------------------------------------------

UPLOAD_FOLDER                       = '/home/xavillobet/Escritorio/BIOINFORMATICA/El meu projecte/0. Apps/0. Sequence & Data Base WebApp v6/uploads'
ALLOWED_EXTENSIONS                  = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'xlsx', 'json', 'csv'}
app.config['MAX_CONTENT_LENGTH']    = 1024 * 1024
app.config['UPLOAD_FOLDER']         = UPLOAD_FOLDER

# # -------------------------------------------------------------------
# # CREATION OF SQL TABLE
# # -------------------------------------------------------------------
# # SQL statements to create the SQL table.

sql.create_table()

# # -------------------------------------------------------------------
# # INSERTING DATA TO SQL TABLE
# # -------------------------------------------------------------------
# # SQL statements to insert random data to the table, for easy testing.

sql.insert_random_data_in_table()

# # -------------------------------------------------------------------
# # ROUTES
# # -------------------------------------------------------------------
# # -------------------------------------------------------------------
# # Index (root)

@app.route('/') 
def index() -> Response:    
    '''
    Shows the index.html.
    Helped with the library my_utils to create the index.
    '''

    index_html: str = my_utils.create_html_index_get_method()    

    return index_html

#----------------------------------------------------------------------
# About this site

@app.route('/get_about.html') 
def about_this_site() -> Response:
    ''''Shows a page with all the inforamtion about each field'''

    about_site_html: str = render_template('get_about.html',
                                                    title = 'FLASK - XLN About')
    return about_site_html

#----------------------------------------------------------------------
# README link

@app.route('/readme.html') 
def readme() -> Response:
    ''''Shows a page with all the inforamtion about each field'''

    # about_site_html: str = render_template('readme.html',
    #                                                 title = 'FLASK - README')
    read_md = Path('README.md').read_text()
    readme = markdown.markdown(read_md)
    return readme

#----------------------------------------------------------------------
# Reverse, Complement and Reverse-Complement Sequence

@app.route('/reverse_and_complement', methods=['GET', 'POST']) 
def reverse_and_complement() -> Response:
    '''
    Calculates the complement sequences and show it in a new page.
    '''
    if request.method == 'GET':

        complement_html_form: str               = render_template('get_post_rev_comp_revcomp.html',
                                                    title = 'Reverse & Complement')
        return complement_html_form

    if request.method == 'POST':

        # -------------------------------------
        if 'reverse' in request.form:
                
                try:
                    input_sequence_rev_form:        str             = request.form['entry_sequence']
                    input_sequence_class_rev_form:  DNASequence     = DNASequence(input_sequence_rev_form)
                    title_rev:                      str             = 'Reverse'
                    reverse_html_output:            str             = my_utils.render_rev_compl_html_template(input_sequence_rev_form, input_sequence_class_rev_form.dna_reverse, title_rev)

                    return reverse_html_output

                except:
                    input_sequence_rev:     str     = request.form['entry_sequence']
                    title_rev_exception:              str     = 'DNA Reverse Errors'
                    rev_exception_html:     str     = my_utils.create_dna_exception(input_sequence_rev, title_rev_exception)

                    return rev_exception_html

        if 'complement' in request.form:

                try:
                    input_sequence_comp_form:           str             = request.form['entry_sequence']
                    input_sequence_comp_form_class:     DNASequence     = DNASequence(input_sequence_comp_form)
                    title_comp:                         str             = 'Complement'
                    complement_html_output:             str             = my_utils.render_rev_compl_html_template(input_sequence_comp_form, input_sequence_comp_form_class.dna_complement, title_comp)

                    return complement_html_output

                except:
                    input_sequence_comp:     str = request.form['entry_sequence']
                    title_comp_exception:              str = 'DNA Complement Errors'
                    comp_exception_html:  str = my_utils.create_dna_exception(input_sequence_comp, title_comp_exception)

                    return comp_exception_html

        if 'rev-compl' in request.form:
                
                try:
                    input_sequence_rev_comp_form:       str             = request.form['entry_sequence']
                    input_sequence_rev_comp_form_class: DNASequence     = DNASequence(input_sequence_rev_comp_form)
                    title_rev_comp:                     str             = 'Reverse-Complement'
                    rev_complement_html_output:         str             = my_utils.render_rev_compl_html_template(input_sequence_rev_comp_form, input_sequence_rev_comp_form_class.dna_reverse_complement, title_rev_comp)

                    return rev_complement_html_output 

                except:
                    input_sequence_rev_comp:     str     = request.form['entry_sequence']
                    title_rev_comp_exception:              str     = 'DNA Reverse-Complement Errors'
                    rev_complement_html_output_exception:  str     = my_utils.create_dna_exception(input_sequence_rev_comp, title_rev_comp_exception)

                    return rev_complement_html_output_exception

        # -------------------------------------
        if 'Reverse' in request.form:
            
            path1: str = 'Reverse_sequence.fa'

            return send_file(path1, as_attachment=True)

        # -------------------------------------
        if 'Complement' in request.form:
            
            path2: str = 'Complement_sequence.fa'

            return send_file(path2, as_attachment=True)

        # -------------------------------------
        if 'Reverse-Complement' in request.form:
            
            path3: str = 'Reverse-Complement_sequence.fa'

            return send_file(path3, as_attachment=True)
        
        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# Random DNA Sequences

@app.route('/get_random_dna', methods=['GET', 'POST'])
def random_dna_sequences() -> Response:
    '''
    Generate a random DNA sequence and show it in a new page.
    Helped with the library my_utils to create the index.
    '''
    if request.method == 'GET':

        random_sequences_form_html: str = render_template('get_post_random_sequences.html',
                                                title   ='Random DNA Sequences')
        return random_sequences_form_html

    if request.method == 'POST':

        # -------------------------------------
        if '30bp' in request.form:

            header30:           list[str]       = ['> Random DNA sequence of 30bp:']
            seq_of_30:          str             = my_utils.create_random_sequences()
            title_30   ='Random'
            #html30: str = my_utils.render_dna_html_template(seq_of_30, title_30)
            seq_of_30_rows:     list[list[str]] = []

            seq_of_30_rows.append(seq_of_30.split('\n'))

            output_data                         = zip(header30, seq_of_30_rows)
            html30:             str             = render_template('get_post_random_sequences.html',
                                                    title   = 'DNA 30bp',
                                                    data    = output_data,)
            
            ### GENERATING FASTA ######################################                    
            file_name_30:       str             = 'random_dna_sequence.fa'
            header_30:          str             = '''> Random DNA sequence of 30bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_30, header_30, html30)
            ########################################################## 

            return html30

        if '60bp' in request.form:

            header60:           list[str]       = ['> Random DNA sequence of 60bp:']
            seq_of_60:          str             = my_utils.create_60bp_dna_sequence()
            seq_of_60_rows:     list[list[str]] = []

            seq_of_60_rows.append(seq_of_60.split('\n'))

            output_data                         = zip(header60, seq_of_60_rows)
            html60:             str             = render_template('get_post_random_sequences.html',
                                                    title   ='DNA 60bp',
                                                    data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_60:       str             = 'random_dna_sequence.fa'
            header_60:          str             = '''> Random DNA sequence of 60bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_60, header_60, html60)
            ########################################################### 
            #  
            return html60

        if '120bp' in request.form:

            headers:                list[str]       = ['> Random DNA sequence of 120bp:']
            seq_of_120:             str             = my_utils.create_120bp_dna_sequence()
            seq_of_120_rows:        str             = my_utils.create_60_digits_format(seq_of_120)
            seq_of_120_splitted:    list[list[str]] = []

            seq_of_120_splitted.append(seq_of_120_rows.split('\n'))

            output_data                             = zip(headers, seq_of_120_splitted)
            html120:                str             = render_template('get_post_random_sequences.html',
                                                        title   ='DNA 120bp',
                                                        data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_120:          str             = 'random_dna_sequence.fa'
            header_120:             str             = '''> Random DNA sequence of 120bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_120, header_120, html120)
            ###########################################################           

            return html120

        if '180bp' in request.form:

            header180:              list[str]       = ['> Random DNA sequence of 180bp:']
            seq_of_180:             str             = my_utils.create_180bp_dna_sequence()
            seq_of_180_rows:        str             = my_utils.create_60_digits_format(seq_of_180)
            seq_of_180_splitted:    list[list[str]] = []

            seq_of_180_splitted.append(seq_of_180_rows.split('\n'))

            output_data                             = zip(header180, seq_of_180_splitted)
            html180:                str             = render_template('get_post_random_sequences.html',
                                                        title   ='DNA 180bp',
                                                        data    = output_data,)
            
            ### GENERATING FASTA ######################################                    
            file_name_180:          str             = 'random_dna_sequence.fa'
            header_180:             str             = '''> Random DNA sequence of 180bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_180, header_180, html180)
            ########################################################### 

            return html180

        if '600bp' in request.form:

            header600:              list[str]       = ['> Random DNA sequence of 600bp:']
            seq_of_600:             str             = my_utils.create_600bp_dna_sequence()
            seq_of_600_rows:        str             = my_utils.create_60_digits_format(seq_of_600)
            seq_of_600_splitted:    list[list[str]] = []

            seq_of_600_splitted.append(seq_of_600_rows.split('\n'))

            output_data                             = zip(header600, seq_of_600_splitted)
            html600:                str             = render_template('get_post_random_sequences.html',
                                                        title   ='DNA 600bp',
                                                        data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_600:          str             = 'random_dna_sequence.fa'
            header_600:             str             = '''> Random DNA sequence of 600bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_600, header_600, html600)
            ###########################################################  

            return html600

        if '6000bp' in request.form:

            header6000:             list[str]       = ['> Random DNA sequence of 6000bp:']
            seq_of_6000:            str             = my_utils.create_6000bp_dna_sequence()
            seq_of_6000_rows:       str             = my_utils.create_60_digits_format(seq_of_6000)
            seq_of_6000_splitted:   list[list[str]] = []

            seq_of_6000_splitted.append(seq_of_6000_rows.split('\n'))

            output_data                             = zip(header6000, seq_of_6000_splitted)
            html6000:               str             = render_template('get_post_random_sequences.html',
                                                    title   ='DNA 6000bp',
                                                    data    = output_data,)
            
            ### GENERATING FASTA ######################################                    
            file_name_6000:         str             = 'random_dna_sequence.fa'
            header_6000:            str             = '''> Random DNA sequence of 6000bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_6000, header_6000, html6000)
            ########################################################### 

            return html6000

        if 'Yours' in request.form:

            lines_amount:           int             = int(request.form['lines'])
            user_seq:               str             = my_utils.user_dna_seq(lines_amount)
            user_seq_rows:          str             = my_utils.create_60_digits_format(user_seq)
            header:                 str             = f'''> Random DNA sequence of {len(user_seq)}:'''
            header_user:            list[str]       = []

            header_user.append(header)
            
            seq_of_user_splitted:   list[list[str]] = []

            seq_of_user_splitted.append(user_seq_rows.split('\n'))

            output_data                             = zip(header_user, seq_of_user_splitted)
            html_user:              str             = render_template('get_post_random_sequences.html',
                                                    title   ='DNA User',
                                                    data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_user:         str             = 'random_dna_sequence.fa'
            header_seq:             str             = f'''> Random DNA sequence of {len(user_seq)}:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_user, header_seq, html_user)
            ########################################################### 

            return html_user

        # -------------------------------------
        if 'download_fasta' in request.form:
            
            path: str = 'random_dna_sequence.fa'

            return send_file(path, as_attachment=True)
        
        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str = request.form['selected_field']
            field_chosen_html:      str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html
        
#----------------------------------------------------------------------
# Random Protein Sequences

@app.route('/get_random_prot', methods=['GET', 'POST'])
def random_protein_sequences() -> Response:
    '''
    Generate a random protein sequence and show it in a new page.
    Helped with the library my_utils to create the index.
    '''
    if request.method == 'GET':    
        headers:    list[str]       = ['> Random Protein sequence of 60bp:']
        seq_of_60:  str             = my_utils.create_60bp_protein_sequence()
        sequences:  list[list[str]] = []

        sequences.append(seq_of_60.split('\n'))

        output_data                 = zip(headers, sequences)
        html60:     str             = render_template('get_post_random_sequences.html',
                                                title   ='Random Protein Sequences',
                                                data    = output_data,)
        return html60

    if request.method == 'POST':

         # -------------------------------------
        if '30bp' in request.form:

            header30:           list[str]       = ['> Random protein sequence of 30bp:']
            seq_of_30:          str             = my_utils.create_30bp_protein_sequence()
            seq_of_30_rows:     list[list[str]] = []

            seq_of_30_rows.append(seq_of_30.split('\n'))

            output_data                         = zip(header30, seq_of_30_rows)
            html30:             str             = render_template('get_post_random_sequences.html',
                                                    title   ='Protein 30bp',
                                                    data    = output_data,)
            
            ### GENERATING FASTA ######################################                    
            file_name_30:       str             = 'random_sequence.fa'
            header_30:          str             = '''> Random DNA sequence of 30bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_30, header_30, html30)
            ########################################################### 

            return html30

        if '60bp' in request.form:

            header60:           list[str]       = ['> Random protein sequence of 60bp:']
            seq_prot_of_60:     str             = my_utils.create_60bp_protein_sequence()
            seq_of_60_rows:     list[list[str]] = []

            seq_of_60_rows.append(seq_prot_of_60.split('\n'))

            output_data                         = zip(header60, seq_of_60_rows)
            html_prot60:        str             = render_template('get_post_random_sequences.html',
                                                    title   ='Protein 60bp',
                                                    data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_60:       str             = 'random_sequence.fa'
            header_60:          str             = '''> Random DNA sequence of 60bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_60, header_60, html_prot60)
            ########################################################### 

            return html_prot60

        if '120bp' in request.form:

            header120:              list[str]       = ['> Random protein sequence of 120bp:']
            seq_of_120:             str             = my_utils.create_120bp_protein_sequence()
            seq_of_120_rows:        str             = my_utils.create_60_digits_format(seq_of_120)
            seq_of_120_splitted:    list[list[str]] = []

            seq_of_120_splitted.append(seq_of_120_rows.split('\n'))

            output_data                             = zip(header120, seq_of_120_splitted)
            html120:                str             = render_template('get_post_random_sequences.html',
                                                        title   ='Protein 120bp',
                                                        data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_120:          str             = 'random_sequence.fa'
            header_120:             str             = '''> Random DNA sequence of 120bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_120, header_120, html120)
            ########################################################### 

            return html120

        if '180bp' in request.form:

            header180:              list[str]       = ['> Random protein sequence of 180bp:']
            seq_of_180:             str             = my_utils.create_180bp_protein_sequence()
            seq_of_180_rows:        str             = my_utils.create_60_digits_format(seq_of_180)
            seq_of_180_splitted:    list[list[str]] = []

            seq_of_180_splitted.append(seq_of_180_rows.split('\n'))

            output_data                             = zip(header180, seq_of_180_splitted)
            html180:                str             = render_template('get_post_random_sequences.html',
                                                    title   ='Protein 180bp',
                                                    data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_180:          str             = 'random_sequence.fa'
            header_180:             str             = '''> Random DNA sequence of 180bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_180, header_180, html180)
            ########################################################### 

            return html180

        if '600bp' in request.form:

            header600:              list[str]       = ['> Random protein sequence of 600bp:']
            seq_of_600:             str             = my_utils.create_600bp_protein_sequence()
            seq_of_600_rows:        str             = my_utils.create_60_digits_format(seq_of_600)
            seq_of_600_splitted:    list[list[str]] = []

            seq_of_600_splitted.append(seq_of_600_rows.split('\n'))

            output_data                             = zip(header600, seq_of_600_splitted)
            html600:                str             = render_template('get_post_random_sequences.html',
                                                        title   ='Protein 600bp',
                                                        data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_600:          str             = 'random_sequence.fa'
            header_600:             str             = '''> Random Protein sequence of 600bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_600, header_600, html600)
            ########################################################### 

            return html600

        if '6000bp' in request.form:

            header6000:             list[str]       = ['> Random protein sequence of 6000bp:']
            seq_of_6000:            str             = my_utils.create_6000bp_protein_sequence()
            seq_of_6000_rows:       str             = my_utils.create_60_digits_format(seq_of_6000)
            seq_of_6000_splitted:   list[list[str]] = []

            seq_of_6000_splitted.append(seq_of_6000_rows.split('\n'))

            output_data                             = zip(header6000, seq_of_6000_splitted)
            html6000:               str             = render_template('get_post_random_sequences.html',
                                                        title   ='Protein 6000bp',
                                                        data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_6000:         str             = 'random_sequence.fa'
            header_6000:            str             = '''> Random Protein sequence of 6000bp:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_6000, header_6000, html6000)
            ###########################################################

            return html6000

        if 'Yours' in request.form:

            lines_amount:           int             = int(request.form['lines'])
            user_seq:               str             = my_utils.user_protein_seq(lines_amount)
            user_seq_rows:          str             = my_utils.create_60_digits_format(user_seq)
            header_user:                 str        = f'''> Random DNA sequence of {len(user_seq)}:'''
            seq_of_user_splitted:   list[list[str]] = []

            seq_of_user_splitted.append(user_seq_rows.split('\n'))

            output_data                             = zip(header_user, seq_of_user_splitted)
            html_user:              str             = render_template('get_post_random_sequences.html',
                                                    title   ='Protein User',
                                                    data    = output_data,)

            ### GENERATING FASTA ######################################                    
            file_name_user:         str             = 'random_sequence.fa'
            header_seq:             str             = f'''> Random DNA sequence of {len(user_seq)}:\n'''

            my_utils.create_fasta_single_sequence_file_from_html(file_name_user, header_seq, html_user)
            ########################################################### 

            return html_user

        # -------------------------------------
        if 'download_fasta' in request.form:
            
            path: str = 'random_sequence.fa'

            return send_file(path, as_attachment=True)

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# DNA to RNA, AinoAcid or Protein

@app.route('/get_translations', methods=['GET', 'POST'])
def translate_to() -> Response:
    '''
    Translate to RNA sequence and show it in a new page.
    '''
    if request.method == 'GET':

        rev_compl_html_form:    str             = render_template('get_post_rna_amacid_protein.html',
                                                    title = 'DNA Translations')
        return rev_compl_html_form 

    if request.method == 'POST':

        if 'rna' in request.form:
            
            try:
                input_sequence_rna_form:        str             = request.form['entry_sequence']
                input_sequence_rna_form_class:  DNASequence     = DNASequence(input_sequence_rna_form)
                rna_sequence:                   str             = input_sequence_rna_form_class.dna_to_rna
                title_rna:                      str             = 'RNA'
                rna_html_output:                str             = my_utils.render_rna_amacid_prot_html_template(input_sequence_rna_form,
                                                                                                                rna_sequence,
                                                                                                                title_rna)
                return rna_html_output

            except:
                input_sequence_rna_exception:   str             = request.form['entry_sequence']
                title_rna_exception:            str             = 'DNA to RNA Errors'
                rna_exception_html:                 str             = my_utils.create_dna_exception(input_sequence_rna_exception, title_rna_exception)

                return rna_exception_html 

        if 'amacid' in request.form:

            try:
                am_input_sequence:         str             = request.form['entry_sequence']
                am_sequence_class:         DNASequence     = DNASequence(am_input_sequence)
                amacid_sequence:           str             = am_sequence_class.dna_to_aminoacid
                title_amacid:              str             = 'AmAcid'
                amacid_html_output:        str             = my_utils.render_rna_amacid_prot_html_template( am_input_sequence,
                                                                                                            amacid_sequence,
                                                                                                            title_amacid)
                return amacid_html_output

            except:
                input_sequence:     str = request.form['entry_sequence']
                title:              str = 'DNA to AmAcid Errors'
                amacid_exception_html:     str = my_utils.create_dna_exception(input_sequence, title)

                return amacid_exception_html 

        if 'protein' in request.form:

            try:
                dna_input_sequence:        str             = request.form['entry_sequence']
                dna_sequence_class:        DNASequence     = DNASequence(dna_input_sequence)
                protein_sequence:          str             = dna_sequence_class.dna_to_protein
                title_protein:             str             = 'Protein'
                protein_html_output:       str             = my_utils.render_rna_amacid_prot_html_template( dna_input_sequence,
                                                                                                            protein_sequence,
                                                                                                            title_protein)
                return protein_html_output
            
            except:
                input_prot_sequence:       str             = request.form['entry_sequence']
                title_prot_eception:       str             = 'DNA to Protein Errors'
                prto_exception_html:       str             = my_utils.create_dna_exception(input_prot_sequence, title_prot_eception)

                return prto_exception_html 

        # -------------------------------------
        if 'RNA' in request.form:
            
            path1: str = 'RNA_sequence.fa'

            return send_file(path1, as_attachment=True)
        
        # -------------------------------------
        if 'AmAcid' in request.form:
            
            path2: str = 'AmAcid_sequence.fa'

            return send_file(path2, as_attachment=True)

        # -------------------------------------
        if 'Protein' in request.form:
            
            path3: str = 'Protein_sequence.fa'

            return send_file(path3, as_attachment=True)

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html    

#----------------------------------------------------------------------
# Sequence Cutter

@app.route('/get_seq_cutter', methods=['GET', 'POST'])
def cut_sequence() -> Response:
    '''
    Cut the entry sequence depending of the parameters entered in the form, and
    extract the sequence range.
    '''
    if request.method == 'GET':

        rev_compl_html_form:    str         = render_template('get_post_cut_comb_pattern.html',
                                                    title = 'DNA Range Extractor')
        return rev_compl_html_form 

    if request.method == 'POST':

        if 'c_c_p' in request.form:
            
            try:
                input_sequence:         str             = request.form['entry_sequence']
                start_cut:              int             = int(request.form['start_cut'])
                end_cut:                int             = int(request.form['end_cut'])
                sequence_class:         DNASequence     = DNASequence(input_sequence)
                cutted_sequence:        str             = my_utils.sequence_cutter(input_sequence, start_cut, end_cut)
                cutted_sequence_rows:   str             = my_utils.create_60_digits_format(cutted_sequence)
                headers:                list[str]       = ['> Entered sequence:',  '> Range extracted sequence:']
                sequences:              list[list[str]] = []

                sequences.append(sequence_class.dna_60digits_sequence.split('\n'))
                sequences.append(cutted_sequence_rows.split('\n'))

                output_data                 = zip(headers, sequences)
                cutted_html_output:     str         = render_template('get_post_cut_comb_pattern.html',
                                                            title           = 'DNA Range Extractor Result',
                                                            data   = output_data,)
                return cutted_html_output

            except:
                input_sequence_exception:     str = request.form['entry_sequence']
                title_cut_exception:              str = 'DNA Range Extractor Errors'
                cut_exception_html:     str = my_utils.create_dna_exception(input_sequence_exception, title_cut_exception)

                return cut_exception_html

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html  

#----------------------------------------------------------------------
# Combine sequences

@app.route('/get_seq_combinator', methods=['GET', 'POST'])
def sequence_combinator() -> Response:
    '''
    Cut the entry sequence depending of the parameters entered in the form, and
    extract the sequence range.
    '''
    if request.method == 'GET':

        rev_compl_html_form:    str         = render_template('get_post_cut_comb_pattern.html',
                                                    title = 'Sequence Combinator')
        return rev_compl_html_form 

    if request.method == 'POST':

        if 'c_c_p' in request.form:
            
            input_sequence_1:         str         = request.form['first_sequence_form']
            input_sequence_2:         str         = request.form['second_sequence_form']
            
            try:
                sequence_class_1:     DNASequence = DNASequence(input_sequence_1)
            
            except:
                input_sequence:     str = request.form['first_sequence_form']
                title:              str = 'DNA Combinator Errors (1st sequence)'
                exception_html:     str = my_utils.create_dna_exception(input_sequence, title)

                return exception_html

            try:
                sequence_class_2:         DNASequence = DNASequence(input_sequence_2)
                final_sequence:           str         = my_utils.combine_sequences(input_sequence_1, input_sequence_2)
                combined_sequence_class:  DNASequence = DNASequence(final_sequence)

                headers:                list[str]       = ['> Entered sequence 1:',  '> Entered sequence 2:', '> Combined sequence:']
                sequences:              list[list[str]] = []

                sequences.append(sequence_class_1.dna_60digits_sequence.split('\n'))
                sequences.append(sequence_class_2.dna_60digits_sequence.split('\n'))
                sequences.append(combined_sequence_class.dna_60digits_sequence.split('\n'))

                output_data                 = zip(headers, sequences)
                combined_html_output:     str         = render_template('get_post_cut_comb_pattern.html',
                                                            title           = 'Sequence Combinator Result',
                                                            data   = output_data,)

                return combined_html_output

            except:
                input_sequence_comb_exception:     str = request.form['second_sequence_form']
                title_comb_exception:              str = 'DNA Combinator Errors (2nd sequence)'
                comb_exception_html:     str = my_utils.create_dna_exception(input_sequence_comb_exception, title_comb_exception)

                return comb_exception_html

        #---------------------
        if 'field_to_go' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# Validate DNA
@app.route('/get_dna_validator', methods=['GET', 'POST'])
def dna_validator() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        valid_dna_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'DNA Validator Form')
        
        return valid_dna_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:

            input_sequence:         str             = request.form['entry_sequence']
            input_sequence_rows:    str             = my_utils.create_60_digits_format(input_sequence)
            valid_sequence:         str             = my_utils.dna_validator(input_sequence)
            headers:                list[str]       = ['> Entered sequence:',  'DNA Validation result:']
            sequences:              list[list[str]] = []

            sequences.append(input_sequence_rows.split('\n'))
            sequences.append(valid_sequence.split('\n'))

            output_data                 = zip(headers, sequences)
            valid_dna_html_output:  str = render_template('get_post_filter_or_validator.html',
                                                        title  = 'DNA Validator',
                                                        data   = output_data,)
            return valid_dna_html_output  

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html    

#----------------------------------------------------------------------
# Validate Protein

@app.route('/get_protein_validator', methods=['GET', 'POST'])
def protein_validator() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        valid_dna_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'Protein Validator Form')
        return valid_dna_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:

            input_sequence:             str             = request.form['entry_sequence']
            input_sequence_rows:        str             = my_utils.create_60_digits_format(input_sequence)
            valid_protein_sequence:     str             = my_utils.protein_validator(input_sequence)
            headers:                    list[str]       = ['> Entered sequence:',  '> Protein Validation result:']
            sequences:                  list[list[str]] = []

            sequences.append(input_sequence_rows.split('\n'))
            sequences.append(valid_protein_sequence.split('\n'))

            output_data                 = zip(headers, sequences)
            valid_dna_html_output:  str = render_template('get_post_filter_or_validator.html',
                                                        title  = 'Protein Validator',
                                                        data   = output_data,)
            return valid_dna_html_output 

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html 

#----------------------------------------------------------------------
# Filter & Validate DNA

@app.route('/get_filter_valid_dna', methods=['GET', 'POST'])
def filter_and_valid_dna() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        valid_dna_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'DNA Filter & Validate Form')
        return valid_dna_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:
        
            input_sequence:             str             = request.form['entry_sequence']
            input_sequence_rows:        str             = my_utils.create_60_digits_format(input_sequence)

            filter_and_valid_seq:       str             = my_utils.dna_filter(input_sequence)
            headers:                    list[str]       = ['> Entered sequence:',  '> DNA filter and validation result:']
            sequences:                  list[list[str]] = []

            sequences.append(input_sequence_rows.split('\n'))
            sequences.append(filter_and_valid_seq.split('\n'))

            output_data                 = zip(headers, sequences)
            valid_dna_html_output:  str = render_template('get_post_filter_or_validator.html',
                                                        title  = 'DNA Filter & Validate',
                                                        data   = output_data,)
            return valid_dna_html_output 

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html 

#----------------------------------------------------------------------
# Filter & Validate Potein

@app.route('/get_filter_valid_protein', methods=['GET', 'POST'])
def filter_and_valid_protein() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        valid_dna_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'Protein Filter & Validate Form')
        return valid_dna_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:
        
            input_sequence:             str             = request.form['entry_sequence']
            input_sequence_rows:        str             = my_utils.create_60_digits_format(input_sequence)
            filter_and_valid_seq:       str             = my_utils.protein_filter(input_sequence)
            headers:                    list[str]       = ['> Entered sequence:',  '> Protein filter and validation result:']
            sequences:                  list[list[str]] = []

            sequences.append(input_sequence_rows.split('\n'))
            sequences.append(filter_and_valid_seq.split('\n'))

            output_data                                 = zip(headers, sequences)
            valid_dna_html_output:      str             = render_template('get_post_filter_or_validator.html',
                                                            title  = 'Protein Filter & Validate',
                                                            data   = output_data,)
            return valid_dna_html_output 

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:                str             = request.form['selected_field']
            field_chosen_html:          str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html 

#----------------------------------------------------------------------
# Find pattern

@app.route('/get_find_pattern', methods=['GET', 'POST'])
def find_pattern_in_sequence() -> Response:

    if request.method == 'GET':
        
        find_pattern_html_form:    str         = render_template('get_post_cut_comb_pattern.html',
                                                    title = 'Pattern Finder')
        return find_pattern_html_form 

    if request.method == 'POST':
        
        if 'c_c_p' in request.form:
            
            try:
                input_sequence:           str               = request.form['first_sequence_form']
                input_patern:             str               = request.form['second_sequence_form']
                sequence_class:           SeqReader         = SeqReader(input_sequence)
                pattern_class:            SeqReader         = SeqReader(input_patern)
                final_sequence:           str               = my_utils.find_pattern_indexes_in_string(sequence_class.raw_sequence, input_patern)
                final_sequence_class:     str               = my_utils.create_60_digits_format(final_sequence)
                headers:                  list[str]         = ['> Entered sequence:',  '> Entered pattern:', '> Patterns found (black rows):']
                sequences:                list[list[str]]   = []

                sequences.append(sequence_class.to_60digits_sequence.split('\n'))
                sequences.append(pattern_class.to_60digits_sequence.split('\n'))
                sequences.append(final_sequence_class.split('\n'))

                output_data                                 = zip(headers, sequences)
                combined_html_output:     str               = render_template('get_post_cut_comb_pattern.html',
                                                                title   = 'Pattern Finder Result',
                                                                data    = output_data,)

                return combined_html_output

            except:
                input_sequence_pattern_exception:   str = request.form['first_sequence_form']
                title_pattern_exception:            str = 'DNA Find Pattern Errors'
                pattern_exception_html:             str = my_utils.create_dna_exception(input_sequence_pattern_exception, title_pattern_exception)

                return pattern_exception_html

        if 'field_to_go' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# DNA Molecular weight

@app.route('/get_dna_molecular_weight', methods=['GET', 'POST'])
def calculate_dna_molecular_weight() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        prot_molecular_weight_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'DNA Molecular Weight')
        
        return prot_molecular_weight_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:

            try:
                input_sequence:         str             = request.form['entry_sequence']
                input_sequence_class:   DNASequence     = DNASequence(input_sequence)
                mol_weight_value:       float           = input_sequence_class.dna_protein_mol_weight_kDa
                header:                 str             = '''> Protein Molecular Weigth value:'''
                valid_dna_html_output:  str             = render_template('get_post_filter_or_validator.html',
                                                            title  = 'DNA Molecular Weight Result',
                                                            header = header,
                                                            value  = mol_weight_value)
                return valid_dna_html_output

            except:
                input_sequence_mw_exception:     str = request.form['entry_sequence']
                title_mw_exception:              str = 'Molecular Weight Errors'
                mw_exception_html:               str = my_utils.create_dna_exception(input_sequence_mw_exception, title_mw_exception)

                return mw_exception_html
                
        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str             = request.form['selected_field']
            field_chosen_html:      str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html 

#----------------------------------------------------------------------
# Protein Molecular weight

@app.route('/get_prot_molecular_weight', methods=['GET', 'POST'])
def calculate_prot_molecular_weight() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        prot_molecular_weight_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'Protein Molecular Weight')
        
        return prot_molecular_weight_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:

            try:
                input_sequence:         str             = request.form['entry_sequence']
                input_sequence_class:   PROTSequence     = PROTSequence(input_sequence)
                mol_weight_value:       float           = input_sequence_class.prot_mol_weight_kDa
                header:                 str             = '''> Protein Molecular Weigth value:'''
                valid_dna_html_output:  str             = render_template('get_post_filter_or_validator.html',
                                                            title  = 'Protein Molecular Weight Result',
                                                            header = header,
                                                            value  = mol_weight_value)
                return valid_dna_html_output

            except:
                input_sequence_mw_exception:     str = request.form['entry_sequence']
                title_mw_exception:              str = 'Molecular Weight Errors'
                mw_exception_html:               str = my_utils.create_dna_exception(input_sequence_mw_exception, title_mw_exception)

                return mw_exception_html
                
        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str             = request.form['selected_field']
            field_chosen_html:      str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html 

#----------------------------------------------------------------------
# DNa to Protein GRAVY

@app.route('/get_dna_to_prot_gravy', methods=['GET', 'POST'])
def calculate_dna_to_protein_gravy() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        protein_gravy_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'Protein Gravy')
        
        return protein_gravy_form

    if request.method == 'POST':
        
        if 'entry_sequence' in request.form:

            try:
                input_sequence:         str             = request.form['entry_sequence']
                input_sequence_class:   DNASequence     = DNASequence(input_sequence)
                gravy_value:            float           = input_sequence_class.dna_protein_gravy
                header:                 str             = '> DNa to Protein Gravy value:'
                valid_dna_html_output:  str             = render_template('get_post_filter_or_validator.html',
                                                            title  = 'DNA to Protein Gravy Result',
                                                            header = header,
                                                            value  = gravy_value)
                return valid_dna_html_output
        
            except:
                input_sequence_gravy_exception:     str = request.form['entry_sequence']
                title_gravy_exception:              str = 'Protein GRAVY Errors'
                gravy_exception_html:               str = my_utils.create_dna_exception(input_sequence_gravy_exception, title_gravy_exception)

                return gravy_exception_html

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str             = request.form['selected_field']
            field_chosen_html:      str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# Protein GRAVY

@app.route('/get_prot_gravy', methods=['GET', 'POST'])
def calculate_protein_gravy() -> Response:
    '''Validate if the entry sequence is a DNA sequence or not.'''

    if request.method == 'GET':

        protein_gravy_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'Protein Gravy')
        
        return protein_gravy_form

    if request.method == 'POST':
        
        if 'entry_sequence' in request.form:

            try:
                input_sequence:         str             = request.form['entry_sequence']
                input_sequence_class:   PROTSequence    = PROTSequence(input_sequence)
                gravy_value:            float           = input_sequence_class.prot_gravy
                header:                 str             = '> Protein Gravy value:'
                valid_dna_html_output:  str             = render_template('get_post_filter_or_validator.html',
                                                            title  = 'Protein Gravy Result',
                                                            header = header,
                                                            value  = gravy_value)
                return valid_dna_html_output
        
            except:
                input_sequence_gravy_exception:     str = request.form['entry_sequence']
                title_gravy_exception:              str = 'Protein GRAVY Errors'
                gravy_exception_html:               str = my_utils.create_dna_exception(input_sequence_gravy_exception, title_gravy_exception)

                return gravy_exception_html

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str             = request.form['selected_field']
            field_chosen_html:      str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# Nucleotides Counter

@app.route('/get_nucleotides_amount', methods=['GET', 'POST'])
def get_nucleotides_amount() -> Response:
    
    if request.method == 'GET':

        protein_gravy_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'Nucleotides Amount')
        
        return protein_gravy_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:

            try:
                input_sequence_nucl:         str             = request.form['entry_sequence']
                input_sequence_nucl_class:   DNASequence     = DNASequence(input_sequence_nucl)
                headers_table:               list[str]       = ['Nucleotide', 'Amount', 'Percent %']
                nucleotides_id:              list[str]       = ['A', 'T', 'C', 'G']
                nucleotides_amount:          list[int]       = [input_sequence_nucl_class.a, input_sequence_nucl_class.t,
                                                                input_sequence_nucl_class.c, input_sequence_nucl_class.g]
                nucleotides_percent:         list[int]       = [input_sequence_nucl_class.a_perc, input_sequence_nucl_class.t_perc,
                                                                input_sequence_nucl_class.c_perc, input_sequence_nucl_class.g_perc]

                table_values = zip(nucleotides_id, nucleotides_amount, nucleotides_percent)
                header:                 str             = '> Nucleotides amount:'
                valid_dna_html_output:  str             = render_template('get_post_filter_or_validator.html',
                                                            title  = 'Nucleotides Amount Result',
                                                            header = header,
                                                            headers_table = headers_table,
                                                            values  = table_values)
                return valid_dna_html_output
        
            except:
                input_sequence:     str = request.form['entry_sequence']
                title:              str = 'Protein GRAVY Errors'
                exception_html:     str = my_utils.create_dna_exception(input_sequence, title)

                return exception_html

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str             = request.form['selected_field']
            field_chosen_html:      str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# Codons Counter

@app.route('/get_codons_amount', methods=['GET', 'POST'])
def get_codons_amount() -> Response:

    if request.method == 'GET':

        codons_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'Codons Amount')
        
        return codons_form

    if request.method == 'POST':

        if 'entry_sequence' in request.form:
            
            try:
                input_sequence_codon:         str             = request.form['entry_sequence']
                input_sequence_ncodon_class:   DNASequence     = DNASequence(input_sequence_codon)
                # headers_table:               list[str]       = ['Nucleotide', 'Amount', 'Percent %']
                # nucleotides_id:              list[str]       = ['A', 'T', 'C', 'G']
                codons_amount:          str      = input_sequence_ncodon_class.dna_codons_amount
                # nucleotides_percent:         list[int]       = [input_sequence_nucl_class.a_perc, input_sequence_nucl_class.t_perc,
                #                                                 input_sequence_nucl_class.c_perc, input_sequence_nucl_class.g_perc]

                # table_values = zip(nucleotides_id, nucleotides_amount, nucleotides_percent)
                header:                 str             = 'RESULT:'
                codons_html_output:  str             = render_template('get_post_filter_or_validator.html',
                                                            title  = 'Codons',
                                                            header = header,
                                                            values  = codons_amount)

                ### GENERATING FASTA ######################################                    
                codon_file_name:       str             = f'''codon_analysis.txt'''
                
                my_utils.write_txt_file(codon_file_name, codons_amount)
                ########################################################### 
                return codons_html_output
        
            except:
                input_sequence:     str = request.form['entry_sequence']
                title:              str = 'Codon Errors'
                exception_html:     str = my_utils.create_dna_exception(input_sequence, title)

                return exception_html

        # -------------------------------------
        if 'Codons' in request.form:
            
            path3: str = 'codon_analysis.txt'

            return send_file(path3, as_attachment=True)

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str             = request.form['selected_field']
            field_chosen_html:      str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#######################################################################
#----------------------------------------------------------------------
# EXPERIMENTAL TOOLS
#----------------------------------------------------------------------
# Data Base form

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/get_qpcr_analysis', methods=['GET', 'POST'])
def qpcr_analysis() -> Response:

    if request.method == 'GET':

        files_names_get: list = []
        for path_file in Path(UPLOAD_FOLDER).glob('*.txt'):
                    files_names_get.append(path_file.stem)

        qpcr_analysis_form:    str         = render_template('get_post_filter_or_validator.html',
                                                    title = 'qPCR File Analysis',
                                                    files = files_names_get)
        
        return qpcr_analysis_form
    
    if request.method == 'POST':

        files_names: list = []
        for path_file in Path(UPLOAD_FOLDER).glob('*.txt'):
                    files_names.append(path_file.stem)

        if 'upload_file' in request.form:
            
            uploaded_file  = request.files['file']            
            
            if uploaded_file.filename == '':

                flash('No selected file')
                return redirect('/get_qpcr_analysis')

            if uploaded_file.filename != '':

                for path_file in Path(UPLOAD_FOLDER).glob('*.txt'):

                    if path_file not in files_names:
                        files_names.append(path_file.stem)

                safe_filename = secure_filename(uploaded_file.filename)
                uploaded_file.save(os.path.join(app.config['UPLOAD_FOLDER'], safe_filename))
                
                uploaded_file_form:  str = render_template('get_post_filter_or_validator.html',
                                                            title = 'qPCR Uploaded File',
                                                            files = files_names)

                return uploaded_file_form

        # -------------------------------------
        if 'show_analysis' in request.form:
            
            file_name_stem: str = request.form['file_names']
            file_name: str = f'''{file_name_stem}.txt'''
            
            my_utils.manage_qpcr_files(file_name, 'blank_samples.txt')

            plot_analysis_html:  str = render_template('get_post_filter_or_validator.html',
                                                        title = 'qPCR Analysis',
                                                        files = files_names)

            return plot_analysis_html

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:            str             = request.form['selected_field']
            field_chosen_html:      str             = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html



#######################################################################
#----------------------------------------------------------------------
# DNA DATA BASE
#----------------------------------------------------------------------
# Data Base form

@app.route('/get_dna_db.html', methods=['GET', 'POST'])
def update_db() -> Response:
    '''
    Updates the table with the data entered in the form.
    '''
    # -------------------------------------
    if request.method == 'GET':
            
        db_html_form:   str         = render_template('get_dna_db.html',
                                        title = 'FLASK - XLN DNA_DB')
        return db_html_form
    
    # -------------------------------------
    if request.method == 'POST':

        if 'entry_sequence' in request.form:

            try:
                input_sequence: str         = request.form['entry_sequence']
                sequence_class: DNASequence = DNASequence(input_sequence)

                # --------------------------------------------------
                # SQL statements to insert user's data to the table.
                # --------------------------------------------------        
                sql.insert_user_data_in_table(sequence_class)
                # --------------------------------------------------

                post_output_html:  str      = render_template('post_output_db.html',
                                                title   = 'SQLite3 - XLN',
                                                text    = 'Sequence inserted!')
                return post_output_html
            
            except:
                input_sequence_db_exception:     str = request.form['entry_sequence']
                title_db_exception:              str = 'DNA Data Base Form Errors'
                db_exception_html:     str = my_utils.create_dna_exception(input_sequence_db_exception, title_db_exception)

                return db_exception_html

        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
# Show Table from Data Base

@app.route('/get_dna_table.html', methods=['GET', 'POST'])
def show_db() -> Response:
    '''
    Shows the Data Base Table with all the updates.
    '''
    # -------------------------------------
    if request.method == 'GET':

        columns_list:   list[str]           = ['Sequence','Length','A Amount','A %','T Amount','T %','C Amount','C %','G Amount','G %','Tm °C','To Protein','Mol_Weight_kDa','Prot GRAVY']
        db_connection:  sqlite3.Connection  = sqlite3.connect('sequences.db')
        cursor_tbl:     sqlite3.Cursor      = db_connection.cursor()

        tbl = cursor_tbl.execute('''SELECT * FROM dna_sequences_table''')
        rows = cursor_tbl.fetchall()

        table_html_get: str                 = render_template('get_dna_table.html',
                                                title = 'DNA Table',
                                                entry_columns = columns_list,
                                                entry_rows = rows)
        db_connection.close()

        return table_html_get

    # -------------------------------------
    if request.method == 'POST':

        # -------------------------------------
        if 'filter_dna' in request.form:

            column_filter:  str         = request.form['selected_column']
            filter_value:   str         = request.form['selected_filter']
            rows_filter:    str         = (request.form['pattern']).upper()
            checks_list:    list[str]   = request.form.getlist('check')
            html_result:    str         = my_utils.filter_table_data(column_filter, filter_value, rows_filter, checks_list)

            return html_result
        
        # -------------------------------------
        if 'dna_status_form' in request.form:

            entered_seq:        str         = request.form['sequence_text']
            sequence_class:     DNASequence = DNASequence(entered_seq)
            dna_status_html:    str         = render_template('post_dna_status_result.html',
                                                        title='DNA Status Analysis',
                                                        dna_analysis=sequence_class.dna_sequence_status)

            return dna_status_html.replace('\n', '<br>')

        # -------------------------------------
        if 'to_download' in request.form:

            # Creating all the possible file types:
            # -------------------------------------
            my_utils.create_files()
            
            # Next code HAS TO BE WRITTEN HERE:
            # -------------------------------------
            selected_format:    str = request.form['selected_format']
            default_name:       str = 'dna_table'            

            # -------------------------------------
            if 'xlsx' in selected_format:
                complete_name = f'{default_name}.xlsx'

            if 'csv' in selected_format:
                complete_name = f'{default_name}.csv'

            if 'json' in selected_format:
                complete_name = f'{default_name}.json'

            if 'html' in selected_format:
                complete_name = f'{default_name}.html'

            path: str = complete_name

            return send_file(path, as_attachment=True)

        # -------------------------------------
        if 'selected_field' in request.form:

            field_to_go:        str = request.form['selected_field']
            field_chosen_html:  str = my_utils.field_to_redirect(field_to_go)

            return field_chosen_html

#----------------------------------------------------------------------
#----------------------------------------------------------------------
# MAIN 
#----------------------------------------------------------------------

if __name__ == "__main__":

    app.run(debug=True)

# ---------------------------------------------------------------------
